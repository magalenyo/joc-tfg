﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySidekick : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
    
        if (other.tag == Constants.Tag_Player)
        {

            Destroy(MaskFollowerBehaviour.instance.gameObject);
        }
    }
}
