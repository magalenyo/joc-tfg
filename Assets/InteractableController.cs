﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class InteractableController : MonoBehaviour
{
    public Interactable _assignedInteractable;
    private Consumer _character;

    private void Start()
    {
        _character = GetComponent<Consumer>();
    }

    /// <summary>
    /// If the assigned interactable is != null intercat with it and return true, else return false.
    /// </summary>
    public bool interact()
    {
        if (_assignedInteractable == null)
            return false;

        _assignedInteractable.interact(_character);
        _assignedInteractable = null;
        return true;
    }

    /// <summary>
    /// Assign the interactable.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.tag == "Interactable")
            _assignedInteractable = other.GetComponent<Interactable>();
        
    }

    /// <summary>
    /// Deletes the assigned interactable.
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Interactable")
            _assignedInteractable = null;
        
    }
}
