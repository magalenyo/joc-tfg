%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: torsoMod
  m_Mask: 00000000010000000100000000000000000000000100000001000000000000000100000000000000000000000000000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/hips
    m_Weight: 1
  - m_Path: Armature/hips/left_leg_1
    m_Weight: 1
  - m_Path: Armature/hips/left_leg_1/left_leg_2
    m_Weight: 1
  - m_Path: Armature/hips/left_leg_1/left_leg_2/left_ankle
    m_Weight: 1
  - m_Path: Armature/hips/left_leg_1/left_leg_2/left_ankle/left_foot
    m_Weight: 1
  - m_Path: Armature/hips/right_leg_1
    m_Weight: 1
  - m_Path: Armature/hips/right_leg_1/right_leg_2
    m_Weight: 1
  - m_Path: Armature/hips/right_leg_1/right_leg_2/right_ankle
    m_Weight: 1
  - m_Path: Armature/hips/right_leg_1/right_leg_2/right_ankle/right_foot
    m_Weight: 1
  - m_Path: Armature/hips/torso
    m_Weight: 1
  - m_Path: Armature/hips/torso/left_arm
    m_Weight: 1
  - m_Path: Armature/hips/torso/left_arm/left_forearm
    m_Weight: 1
  - m_Path: Armature/hips/torso/left_arm/left_forearm/left_hand_1
    m_Weight: 1
  - m_Path: Armature/hips/torso/left_arm/left_forearm/left_hand_1/left_hand_2
    m_Weight: 1
  - m_Path: Armature/hips/torso/left_arm/left_forearm/left_hand_1/left_hand_2/left_hand_3
    m_Weight: 1
  - m_Path: Armature/hips/torso/neck
    m_Weight: 1
  - m_Path: Armature/hips/torso/neck/head
    m_Weight: 1
  - m_Path: Armature/hips/torso/right_arm
    m_Weight: 1
  - m_Path: Armature/hips/torso/right_arm/right_forearm
    m_Weight: 1
  - m_Path: Armature/hips/torso/right_arm/right_forearm/right_hand_1
    m_Weight: 1
  - m_Path: Armature/hips/torso/right_arm/right_forearm/right_hand_1/right_hand_2
    m_Weight: 1
  - m_Path: Armature/hips/torso/right_arm/right_forearm/right_hand_1/right_hand_2/right_hand_3
    m_Weight: 1
  - m_Path: Body
    m_Weight: 1
