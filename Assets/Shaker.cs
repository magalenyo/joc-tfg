﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple shaker for chests.
/// </summary>

[RequireComponent(typeof(Collider))]
public class Shaker : MonoBehaviour
{

    public GameObject _chestGO;
    private ChestBehaviour _chest;

    private void Start()
    {
        _chest = _chestGO.GetComponent<ChestBehaviour>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            _chest.shake();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            _chest.stopShake();
    }
}
