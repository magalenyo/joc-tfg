﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public int sceneIndex;
    public GameObject _loadingScreen;
    public Slider _slider;
    public TextMeshProUGUI _progressText;
    public static SceneController instance;
    private bool _alreadyLoading = false;

    private void Awake()
    {
        DontDestroyOnLoad(transform.root.gameObject);
        if (instance == null) instance = this;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Constants.Tag_Player && !_alreadyLoading)
        {
            _alreadyLoading = true;
            loadByIndex(sceneIndex);
        }
    }

    public void loadByIndex(int sceneIndex)
    {
        StartCoroutine(loadAsynchronously(sceneIndex));
    }

    //coroutine
    //les scenes es carreguen de 0 a 0.9 els assets , i de 0.9 a 1, es activation, on s'esborra lo que no necessitem i es substitueix per les noves
    IEnumerator loadAsynchronously(int sceneIndex)
    {
        UIEventHandler.disableBasicGUI();
        _loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);   //per a que no es quedi a 90%

            _slider.value = progress;
            _progressText.text = ((int)progress * 100) + "%";

            yield return null;  //wait a frame 
        }

        _loadingScreen.SetActive(false);
        UIEventHandler.enableBasicGUI();

        Destroy(this.gameObject);
    }
}
