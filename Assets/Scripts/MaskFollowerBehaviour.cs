﻿using UnityEngine;

public class MaskFollowerBehaviour : MonoBehaviour
{
    public static MaskFollowerBehaviour instance;

    private PlayerController _player;
    public Vector3 _offset;
    public Vector3 desiredPosition;

    private CharacterController _charController;

    public float _smooth;
    public float _inc;

    private float _dif;
    private float _sign;
    private double _maxH;
    private double _minH;
   
    private float _acum;

    private System.Random _rnd;

    //Maxim i minim de l'interval que representa l'alçada maxima que pot assolir la mascara.
    public double imaxH_Max,imaxH_Min;

    //Maxim i minim de l'interval que representa l'alçada minima que pot assolir la mascara.
    public double iminH_Max, iminH_Min;

    public float q;

    private void Awake()
    {
        DontDestroyOnLoad(transform.root.gameObject);
    }

    void Start()
    {
        instance = this;
      
        _player = PlayerController.instance;

        transform.position = _player.transform.position + _offset;
        _charController = _player.GetComponent<CharacterController>();
        _dif = 0;
        _sign = -1;
        _acum = 0;
        _inc = 0.0025f;
        _rnd = new System.Random();
        _maxH = _rnd.NextDouble() * (imaxH_Max - imaxH_Min) + imaxH_Min;
        _minH = _rnd.NextDouble() * (iminH_Max - iminH_Min) + iminH_Min;

        imaxH_Max = 0.1;
        imaxH_Min = 0;

        iminH_Max = -0.3;
        iminH_Min = -0.1;
        q = 145;
    }

    void Update()
    {

        if (_player.transform.forward.x > 0)
        {
            desiredPosition = new Vector3(_player.transform.position.x, _player.transform.position.y + _charController.height, _player.transform.position.z) + _offset;
            transform.rotation = Quaternion.Euler(Vector3.up * q);
           
        }
        else
        {
            desiredPosition = new Vector3(_player.transform.position.x, _player.transform.position.y + _charController.height, _player.transform.position.z) - _offset;
            transform.rotation = Quaternion.Euler(Vector3.up * -q);
        }
        desiredPosition.y = desiredPosition.y + (_acum);
        _acum += _inc * _sign;
 
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _smooth * Time.deltaTime); //ha de tenir un smooth major (10f)
        transform.position = smoothedPosition;
      

        _dif = transform.position.y - (_player.transform.position.y + _charController.height);
       

        if (_dif >= _maxH){          
            _sign = -1;
            _minH = _rnd.NextDouble() * (iminH_Max - iminH_Min) + iminH_Min;
        }
        else if (_dif <= _minH) {
            _sign = 1;
            _maxH = _rnd.NextDouble() * (imaxH_Max - imaxH_Min) + imaxH_Min;
        }

    }   
}
