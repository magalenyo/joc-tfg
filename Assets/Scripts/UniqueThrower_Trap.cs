﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ProjectileController))]
public class UniqueThrower_Trap : Trap
{
    public string ProjectileTag = "KunaiTrap";
    private ProjectileController _projectileController;
    private Vector3 _shotPoint;

    private void Start()
    {
        _projectileController = GetComponent<ProjectileController>();
        _projectileController.setOwner(this.gameObject);
        _shotPoint = transform.position;
    }

    public override void activateTrap()
    {
        _projectileController.shoot(Resources.Load<GameObject>(ProjectileTag), _shotPoint);
    }

}
