﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class SkillTree : MonoBehaviour
{
    [Header("Panels")]
    private PlayerController _player;
    [SerializeField] private GameObject _statsPanel;
    [SerializeField] private GameObject _spellsPanel;

    private Dictionary<string, int[]> _upgradedStats;     //[0] -> permanent skill points, [1] -> current skill points
    private Dictionary<string, int[]> _upgradedSpells;    //[0] -> permanent skill points, [1] -> current skill points
    private Dictionary<string, GameObject> _statsObjects;
    private Dictionary<string, GameObject> _spellsObjects;

    private Dictionary<string, float> _increaseStatFactor;
    private Dictionary<string, float> _increaseSpellFactor;

    #region Index array helpers
    private const int permanentSP = 0;
    private const int currentSP = 1;

    private const int prefabPointsUsed = 0;
    private const int prefabButtons = 1;
    private const int prefabItem = 2;
    private const int prefabNewValue = 3;
    #endregion

    void Start()
    {
        _player = PlayerController.instance;

        initializeStats();
        initializeSpells();
        initializeStatFactor();
        initializeSpellFactor();
    }

    void Update()
    {

    }

    #region Initializations
    private void initializeStats()
    {
        _upgradedStats = new Dictionary<string, int[]>();
        _statsObjects = new Dictionary<string, GameObject>();
        foreach (KeyValuePair<string, float> kvp in _player.getStats()._characterStats)
        {
            if (kvp.Key != Constants.currentHealth)
            {
                int[] aux = { 0, 0 };
                _upgradedStats.Add(kvp.Key, aux);

                GameObject go = Instantiate(Resources.Load<GameObject>("UI/StatItemWithButtons"));
                
                //Skill point text Assignment
                TextMeshProUGUI tmsp = go.transform.GetChild(prefabPointsUsed).GetComponent<TextMeshProUGUI>();
                tmsp.text = "";

                //Button script Assignment
                Button[] buttons = go.transform.GetChild(prefabButtons).GetComponentsInChildren<Button>();
                buttons[0].onClick.AddListener(() => plusClickedStats(kvp.Key));
                buttons[1].onClick.AddListener(() => minusClickedStats(kvp.Key));

                //Item Assignment
                TextMeshProUGUI[] tm = go.transform.GetChild(prefabItem).GetComponentsInChildren<TextMeshProUGUI>();
                tm[0].text = kvp.Key;
                tm[1].text = kvp.Value.ToString();

                //New value Assignment
                TextMeshProUGUI tmnv = go.transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>();
                tmnv.text = "";

                //Parent Assignment
                go.transform.SetParent(_statsPanel.transform);

                _statsObjects.Add(kvp.Key, go);
            }
        }
    }

    private void initializeSpells()
    {
        _upgradedSpells = new Dictionary<string, int[]>();
        _spellsObjects = new Dictionary<string, GameObject>();
        foreach (Spell spell in _player._spellController._allSpells)
        {
            int[] aux = { 0, 0 };
            _upgradedSpells.Add(spell.GetType().ToString(), aux);

            GameObject go = Instantiate(Resources.Load<GameObject>("UI/SpellItemWithButtons"));
            
            //Skill point text Assignment
            TextMeshProUGUI tmsp = go.transform.GetChild(prefabPointsUsed).GetComponent<TextMeshProUGUI>();
            tmsp.text = "Cost: " + spell.getCostSkillPoint();

            //Button script Assignment
            Button[] buttons = go.transform.GetChild(prefabButtons).GetComponentsInChildren<Button>();
            buttons[0].onClick.AddListener(() => plusClickedSpells(spell.GetType().ToString()));
            buttons[1].onClick.AddListener(() => minusClickedSpells(spell.GetType().ToString()));

            //Item assignment
            TextMeshProUGUI[] tm = go.transform.GetChild(prefabItem).GetComponentsInChildren<TextMeshProUGUI>();
            tm[0].text = spell.getName();              //nom text
            tm[1].text = "Level: " + 0;                //level text

            Image[] imgs = go.transform.GetChild(prefabItem).GetComponentsInChildren<Image>();
            imgs[0].sprite = Resources.Load<Sprite>("UI/SpellImage/" + spell.GetType());      //button image

            //New value assignment
            TextMeshProUGUI tmnv = go.transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>();
            tmnv.text = "";

            //Parent Assignment
            go.transform.SetParent(_spellsPanel.transform);

            _spellsObjects.Add(spell.GetType().ToString(), go);
        }
    }

    private void initializeStatFactor()
    {
        _increaseStatFactor = new Dictionary<string, float>();
        _increaseStatFactor.Add(Constants.maxHealth, Constants.Factor_Player_maxHealth);
        _increaseStatFactor.Add(Constants.resMagic, Constants.Factor_Player_resMagic);
        _increaseStatFactor.Add(Constants.resPhys, Constants.Factor_Player_resPhys);
        _increaseStatFactor.Add(Constants.strength, Constants.Factor_Player_strength);
        _increaseStatFactor.Add(Constants.moveSpeed, Constants.Factor_Player_moveSpeed);
        _increaseStatFactor.Add(Constants.cooldown, Constants.Factor_Player_cooldown);
        _increaseStatFactor.Add(Constants.magic, Constants.Factor_Player_magic);
    }

    private void initializeSpellFactor()
    {
        _increaseSpellFactor = new Dictionary<string, float>();
        _increaseSpellFactor.Add(Constants.Spell_fireball, Constants.Factor_Spell_Fireball);
        _increaseSpellFactor.Add(Constants.Spell_IceStorm, Constants.Factor_Spell_IceStorm);
    }

    #endregion

    #region Click Events
    private void plusClickedStats(string key)
    {
        if(ExperienceSystem.canUseSkillPoints())        //queden skill points per usar
        {
            _upgradedStats[key][currentSP]++;
            string spText = "";
            if (_upgradedStats[key][permanentSP] > 0)
            {
                spText = _upgradedStats[key][permanentSP] + " | " + _upgradedStats[key][currentSP];
                //_statsObjects[key].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _upgradedStats[key][permanentSP] + "+";
            }
            else spText = _upgradedStats[key][currentSP].ToString();

            _statsObjects[key].transform.GetChild(prefabPointsUsed).GetComponent<TextMeshProUGUI>().text = spText;
            float value = getNewStatValue(key);
            _statsObjects[key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = value == 0 ? "" : value.ToString();
            ExperienceSystem.decreaseSkillPoint(1);
            UIEventHandler.useSkillPoint();
        }
    }

    private void minusClickedStats(string key)
    {
        if(_upgradedStats[key][currentSP] > 0)
        {
            _upgradedStats[key][currentSP]--;
            string spText = "";
            if(_upgradedStats[key][permanentSP] > 0)
            {
                spText = _upgradedStats[key][permanentSP].ToString();
                if (_upgradedStats[key][currentSP] > 0)
                {
                    spText += " | ";
                }
            }
            if (_upgradedStats[key][currentSP] > 0)
            {
                spText += _upgradedStats[key][currentSP].ToString();
            }

            _statsObjects[key].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = spText;
            float value = getNewStatValue(key);
            _statsObjects[key].transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = value == 0 ? "" : value.ToString();

            ExperienceSystem.increaseSkillPoint(1);
            UIEventHandler.useSkillPoint();
        }
    }

    private void plusClickedSpells(string key)
    {
        Spell spell = _player.getSpell(key);
        if (ExperienceSystem.canUpgradeSkillWithCost(spell.getCostSkillPoint()))    //si pot desbloquejar
        {
            if (_upgradedSpells[key][permanentSP] == 0 && _upgradedSpells[key][currentSP] == 0)     //si es pot desactivar candau
            {
                (_spellsObjects[key].transform.GetChild(prefabItem).GetComponentsInChildren<Image>())[1].gameObject.SetActive(false);
                //(_spellsObjects[key].transform.GetChild(prefabItem).GetComponentsInChildren<Image>())[1].sprite = null;
            }
            _upgradedSpells[key][currentSP] += spell.getCostSkillPoint();        //afegir skill points a current
            float value = getNewSpellValue(key, spell.getCostSkillPoint());
            _spellsObjects[key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = value == 0 ? "" : value.ToString();   //new value
            (_spellsObjects[key].transform.GetChild(prefabItem).GetComponentsInChildren<TextMeshProUGUI>())[1].text =
                "Level: " + (int) (_upgradedSpells[key][currentSP] + _upgradedSpells[key][permanentSP]) / spell.getCostSkillPoint();    //canviar el text de level
            ExperienceSystem.decreaseSkillPoint(spell.getCostSkillPoint());
            UIEventHandler.useSkillPoint();
        }
        
    }

    private void minusClickedSpells(string key)
    {
        Spell spell = _player.getSpell(key);
        int level = 0;
        if(_upgradedSpells[key][currentSP] > 0)                              //si esta guardat a current
        {
            ExperienceSystem.increaseSkillPoint(spell.getCostSkillPoint());  //retornar el cost
            _upgradedSpells[key][currentSP] -= spell.getCostSkillPoint();    //treure els skill points de current

            if (_upgradedSpells[key][currentSP] > 0) level += _upgradedSpells[key][currentSP] / spell.getCostSkillPoint();      //comptador dels currents
            if (_upgradedSpells[key][permanentSP] > 0) level += _upgradedSpells[key][permanentSP] / spell.getCostSkillPoint();  //comptador dels permanents

            if (level == 0)                                                     //si s'ha d'activar el candau
            {
                (_spellsObjects[key].transform.GetChild(prefabItem).GetComponentsInChildren<Image>(true))[1].gameObject.SetActive(true);
                _spellsObjects[key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = "";
            }
            else
            {
                float value = getNewSpellValue(key, spell.getCostSkillPoint());
                _spellsObjects[key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = value == 0 ? "" : value.ToString();   //new value
            }

            (_spellsObjects[key].transform.GetChild(prefabItem).GetComponentsInChildren<TextMeshProUGUI>())[1].text = "Level: " + level;
            UIEventHandler.useSkillPoint();
        }


    }

    private float getNewStatValue(string key)
    {
        return _increaseStatFactor[key] * _upgradedStats[key][1];
        
    }

    private float getNewSpellValue(string key, float cost)
    {
        return _increaseSpellFactor[key] * _upgradedSpells[key][1] / cost;
    }
    #endregion

    public void save()
    {
        foreach(KeyValuePair<string,int[]> kvp in _upgradedStats)
        {
            _player.getStats().increaseStat(kvp.Key, getNewStatValue(kvp.Key));           //increase en cas de que pogui
            _upgradedStats[kvp.Key][permanentSP] += _upgradedStats[kvp.Key][currentSP];   //punts actuals a permanents
            _upgradedStats[kvp.Key][currentSP] = 0;                                       //punts actuals a 0
            
            if (_upgradedStats[kvp.Key][permanentSP] != 0)
            {
                _statsObjects[kvp.Key].transform.GetChild(prefabPointsUsed).GetComponent<TextMeshProUGUI>().text = _upgradedStats[kvp.Key][permanentSP].ToString();  //mostrar el numero de skill points utilitzats   
            }
            _statsObjects[kvp.Key].transform.GetChild(prefabItem).GetComponentsInChildren<TextMeshProUGUI>()[1].text = _player.getStat(kvp.Key).ToString();                           //stat value
            _statsObjects[kvp.Key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = "";
        }

        foreach (KeyValuePair<string, int[]> kvp in _upgradedSpells)
        {
            _upgradedSpells[kvp.Key][permanentSP] += _upgradedSpells[kvp.Key][currentSP];   //punts actuals a permanents
            _upgradedSpells[kvp.Key][currentSP] = 0;                                        //punts actuals a 0
            _player._spellController.setLevelSpell(kvp.Key, _upgradedSpells[kvp.Key][permanentSP] / _player.getSpell(kvp.Key).getCostSkillPoint());     //Miguel: 01/06/2019 sets the level of the current spell
            //_player.getStats().increaseStat(kvp.Key, getNewSpellValue(kvp.Key, _player.getSpell(kvp.Key).getCostSkillPoint()));   //guarda el nou valor   //Miguel: 01/06/2019 comentat

            //if (_upgradedSkills[kvp.Key][permanentSP] != 0) _spellsObjects[kvp.Key].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _upgradedStats[kvp.Key][permanentSP].ToString();
        }


        UIEventHandler.useSkillPoint();
    }

    public void emptySkillPoints()
    {
        foreach (KeyValuePair<string, int[]> kvp in _upgradedStats)
        {
            ExperienceSystem.increaseSkillPoint(_upgradedStats[kvp.Key][currentSP]);
            _upgradedStats[kvp.Key][currentSP] = 0;                                       //punts actuals a 0
            if (_upgradedStats[kvp.Key][permanentSP] != 0) _statsObjects[kvp.Key].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _upgradedStats[kvp.Key][permanentSP].ToString();
            float value = getNewStatValue(kvp.Key);                                         //new value
            _statsObjects[kvp.Key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = value == 0 ? "" : value.ToString();     //refresh new value
        }

        foreach (KeyValuePair<string, int[]> kvp in _upgradedSpells)
        {
            ExperienceSystem.increaseSkillPoint(_upgradedSpells[kvp.Key][currentSP]);       //punts actuals a permanents
            _upgradedSpells[kvp.Key][currentSP] = 0;                                        //punts actuals a 0
            Spell spell = _player.getSpell(kvp.Key);
            if(_upgradedSpells[kvp.Key][permanentSP] > 0)
            {
                (_spellsObjects[kvp.Key].transform.GetChild(prefabItem).GetComponentsInChildren<TextMeshProUGUI>())[1].text =
                "Level: " + (int)(_upgradedSpells[kvp.Key][permanentSP]) / spell.getCostSkillPoint();
            }
            float value = getNewSpellValue(kvp.Key, spell.getCostSkillPoint());
            _spellsObjects[kvp.Key].transform.GetChild(prefabNewValue).GetComponent<TextMeshProUGUI>().text = value == 0 ? "" : value.ToString();
        }

        UIEventHandler.useSkillPoint();
    }

}
