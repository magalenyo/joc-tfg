﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spell controllers saves the spells information
/// </summary>
public class SpellController
{

    public int Primary = 0;               //Key of primary spell
    public int Secondary = 1;             //Key of secondary spell

    public List<Spell> _allSpells;              //All spells
    public int _current;                        //current spell index
    private List<float> _cooldowns;             //cooldowns of the spells

    /// <summary>
    /// Initializes spell array
    /// </summary>
    public SpellController(List<Spell> allSpells)
    {
        _allSpells = allSpells;
        _cooldowns = new List<float>();

        //ini. cooldown
        for (int i = 0; i < allSpells.Count; i++)
            _cooldowns.Add(Time.time);

    }
  
    /// <summary>
    /// Saves selected spell to current assigned spells
    /// type = primary/secundary
    /// </summary>
    public void assignSpell(int type,int newSpell)
    {
        if (type == 0)
            Primary = newSpell;
        else
            Secondary = newSpell;
    }
    /// <summary>
    /// Casts the current spell
    /// </summary>
    public void castCurrent()
    {
        if (canCastCurrent())
        {
            _allSpells[_current].cast();
            _cooldowns[_current] = Time.time + _allSpells[_current].cooldown();
        }
       
    }

    /// <summary>
    /// Returns true if the current spell can be cast
    /// </summary>
    public bool canCastCurrent()
    {
        return _cooldowns[_current] <= Time.time && _allSpells[_current].unlocked();
    }

 
    /// <summary>
    /// Casts the index spell
    /// </summary>
    public void cast(int index)
    {
        _allSpells[index].cast();
    }

    /// <summary>
    /// Returns the spell from an index
    /// </summary>
    public Spell getCurrentSpell()
    {
        return _allSpells[_current];
    }

    /// <summary>
    /// Returns the type of the current spell
    /// </summary>
    public string currentType()
    {
        return _allSpells[_current].getType();
    }

    /// <summary>
    /// Returns a spell by it's type if found, else returns null
    /// </summary>
    public Spell getSpellByType(string name)
    {
        foreach (Spell spell in _allSpells)
        {
            if (spell.GetType().ToString() == name) return spell;
        }
        return null;
    }

    /// <summary>
    /// Adds the given spell
    /// </summary>
    public void addSpell(Spell _newSpell)
    {
        _allSpells.Add(_newSpell);
        _cooldowns.Add(Time.time);
    }

    public void setLevelSpell(string name, int level)
    {
        foreach (Spell spell in _allSpells)
        {
            if (spell.GetType().ToString() == name)
            {
                spell.setLevel(level);
            }
        }
    }
}
