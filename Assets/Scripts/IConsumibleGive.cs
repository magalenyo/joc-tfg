﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Consumible))]
public class IConsumibleGive : Interactable
{
   
    private Consumible _consumible;
    public int amount = 1;

    public void Start()
    { 
    
        _consumible = GetComponent<Consumible>();
    }
    public override void interact(Consumer _character)
    {
        _character.giveObject(_consumible,amount);
        gameObject.SetActive(false);
    }

}
