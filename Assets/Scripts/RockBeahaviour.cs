﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBeahaviour : MonoBehaviour
{
    public float force = 0.3f;
    public Rigidbody _rb;

    // Start is called before the first frame update
    void Start()
    {
        force = 0.3f;
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       _rb.AddForce(0, 0, -force, ForceMode.Impulse);
    }
}
