﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class leverController : Interactable
{
    public GameObject _activator;
    public DoorController _door;
    public bool _oneUse = true;
    private bool _used = false;
    private float _rotValue = 180f;


    private AudioSource _audioSource;
    [SerializeField] private AudioClip _clipOnClick;
    private Collider _iCollider;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _iCollider = GetComponent<Collider>();
        _used = false;
    }

    public override void interact(Consumer _character)
    {
        changeStat();

        //Rotation
        _activator.transform.Rotate(new Vector3(0,_rotValue,0),Space.World);
        //Sound
        if (_clipOnClick != null) _audioSource.PlayOneShot(_clipOnClick);
    }
 

    private void changeStat()
    {
        try
        {
            _door.changeStat();
            if (_oneUse) _iCollider.enabled = false;
        }
        catch (NullReferenceException e)
        {
            Console.WriteLine("Error: The object to change state was not assigned", e.Source);
        }
    }

}
