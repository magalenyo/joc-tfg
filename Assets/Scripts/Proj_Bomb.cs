﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proj_Bomb : Projectile
{
    public float _radius = 5f;

    private float _countdown = 1.2f;
    private bool _hasExploded = false;
    private bool _alreadyHit = false;

    public GameObject _explosionEffect;

    public AudioClip _ACexplosion;

    public Proj_Bomb()
    {
        _lifeTimeLong = Constants.Lt_Long_ProjBomb;
        _lifeTimeShort = Constants.Lt_Short_ProjBomb;
        _projectileDmg = Constants.Dmg_ProjBomb;
        _shootForce = Constants.Speed_ProjBomb;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _countdown -= Time.deltaTime;
        if(_countdown <= 0 && !_hasExploded)
        {
            _hasExploded = true;
            explode();
        }
    }

    private void FixedUpdate()
    {
        if (transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        }
    }

    private void explode()
    {
        GameObject go = Instantiate(_explosionEffect, transform.position, transform.rotation);
        AudioSource newAudioSource = go.AddComponent<AudioSource>();
        newAudioSource.PlayOneShot(_ACexplosion);
        Destroy(go, 2);

        Collider[] colliders = Physics.OverlapSphere(transform.position, _radius);

        foreach(Collider col in colliders)
        {
            if(col.tag == _targetTag && !_alreadyHit)
            {
                _alreadyHit = true;
                takeDamageEffect(col);
                makeDamage(col.GetComponent<Character>());
                _hitEnemy = true;
            }
        }

        Destroy(gameObject);
    }

    protected override void OnCollisionEnter(Collision collision) { }

    protected override void stick(Collision collision) { }
}
