﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class LoadSceneOnClick : MonoBehaviour
{
    public GameObject _loadingScreen;
    public Slider _slider;
    public TextMeshProUGUI _progressText;
    public static LoadSceneOnClick instance;
    public GameObject GUI;
    public bool deleteGUI = false;
    private bool aux;

    public void loadByIndex(int sceneIndex)
    {
        //if (GodController.instance != null && !GodController.instance._firstTime)
        //{
        //    destroyAll();
        //    GodController.instance._firstTime = false;
        //    StaticResources.firstTime = false;

        //}
        //if(SceneManager.GetActiveScene().buildIndex != 0)
        //{

        //    //destroyAll();
        //}
        //aux = StaticResources.firstTime;
        destroyAll();

        StartCoroutine(loadAsynchronously(sceneIndex));
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
  
    }

    private void destroyAll()
    {
        if (PlayerController.instance != null) Destroy(PlayerController.instance.gameObject);
        if (MaskFollowerBehaviour.instance != null) Destroy(MaskFollowerBehaviour.instance.gameObject);
        if (SceneController.instance != null) Destroy(SceneController.instance.gameObject);
        //foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
        //{
        //    if(go.tag == "Player" && go.tag == "Mascarea" &&  ) Destroy(go);
        //}
            
    }

    //coroutine
    //les scenes es carreguen de 0 a 0.9 els assets , i de 0.9 a 1, es activation, on s'esborra lo que no necessitem i es substitueix per les noves
    IEnumerator loadAsynchronously(int sceneIndex)
    {
        _loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);   //per a que no es quedi a 90%

            _slider.value = progress;
            _progressText.text = ((int) progress * 100) + "%";

            yield return null;  //wait a frame 
        }

        if (deleteGUI)
        {
            Destroy(GUI);
        }
        if (SceneManager.GetActiveScene().buildIndex > 1) PlayerController.instance.teleportTo(StaticResources.getLevelPosition(sceneIndex));
        _loadingScreen.SetActive(false);

    }
}
