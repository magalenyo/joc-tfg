﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Enemy : Character
{
    //protected virtual void Start()
    //{
    //    base.Start();
    //    //Physics.IgnoreLayerCollision(10, 12);

    //}


    public override void respawn(Vector3 Position)
    {
        resetStats();
        teleportTo(Position);
        gameObject.SetActive(true);
    }

    protected override void die()
    {
        ExperienceSystem.addExperience(getStat(Constants.experience));
        ExperienceSystem.levelUp();
        SoulsSystem.addSoul((int)getStat(Constants.soulCounter));
        UIEventHandler.experienceGained(ExperienceSystem._playerExperience, ExperienceSystem._requiredExperience, ExperienceSystem._playerLevel);
        UIEventHandler.soulCollected(SoulsSystem._soulCounter);
        castSoul();
        _actions[_dead] = true;
        //_animLayer["Die"] = 1;
        gameObject.SetActive(false);
    }

    public float getExperience()
    {
        return getStat("experience");
    }

    private void castSoul()
    {
        GameObject go = Instantiate(Resources.Load<GameObject>("SoulOrb/OrbSoulFire"),transform.position,Quaternion.identity);
    }

    public void setLevel(int level)
    {
        float factor = level *0.10f;
   
        _stats.increaseStat(Constants.maxHealth, (float) Math.Round (_stats.getStat(Constants.maxHealth) * factor));
        _stats.increaseStat(Constants.currentHealth, (float)Math.Round(_stats.getStat(Constants.currentHealth) * factor));
        _stats.increaseStat(Constants.resMagic, (float)Math.Round(_stats.getStat(Constants.resMagic) * factor));
        _stats.increaseStat(Constants.resPhys, (float)Math.Round(_stats.getStat(Constants.resPhys) * factor));
        _stats.increaseStat(Constants.strength, (float)Math.Round(_stats.getStat(Constants.strength) * factor));
        _stats.increaseStat(Constants.magic, (float)Math.Round(_stats.getStat(Constants.magic) * factor));

        //_stats.increaseStat(Constants.moveSpeed, _stats.getStat(Constants.moveSpeed) * factor));
        //_stats.increaseStat(Constants.cooldown, factor);
    }

    protected bool isPlayerDead()
    {
        return PlayerController.instance.isDead() || PlayerController.instance.isDying();
    }
}
