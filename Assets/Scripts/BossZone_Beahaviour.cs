﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class BossZone_Beahaviour : MonoBehaviour
{
    public string _bossName;                //Name of the boss prefab in Resources/Boss
    public Transform _spawnPoint;           //Boss spawn position 
    
    private List<GameObject> _wallList;     //List of all walls inside this gameObject    

    private Character _boss;                
    private static Object _bossPrefab;

    private PlayerController _player;              
    private bool activeZone;                //Indicates when the bossZone is active

    private void Awake()
    {
        _wallList = new List<GameObject>();
    }

    void Start()
    {
        activeZone = false;
        getAllWalls();
        deactivateWalls();
        _player = PlayerController.instance;

       _bossPrefab = Resources.Load<GameObject>("boss/" + _bossName);
  
    }
    private void Update()
    {
        if (_boss != null && _boss.isDead())
        {
            deactivateWalls();
            gameObject.SetActive(false); //desactivem tot l'objecte per no tenir que controlar res mes.
        }
           

        if (activeZone && _player.isRespawning())
            restartBossZone();
        
    }

    /// <summary>
    /// Get all the gameObjects inside of this with the tag "invWall".
    /// </summary>
    private void getAllWalls()
    {
        foreach (Transform child in transform)
            if (child.tag == "invWall")
                _wallList.Add(child.gameObject);
    }

    /// <summary>
    /// Active all the gameObjects with the tag "invWall".
    /// </summary>
    private void activeWalls()
    {
        foreach (GameObject wall in _wallList)
        {
            activeZone = true;
            instantiateBoss();
            wall.SetActive(true);
        }
    }

    /// <summary>
    /// Deactive all the gameObjects with the tag "invWall".
    /// </summary>
    private void deactivateWalls()
    {
        foreach (GameObject wall in _wallList)
        {
            wall.SetActive(false);
        }
    }

    /// <summary>
    /// When the gameObject withe the tag "Player" collides active all walls and instantiate the boss.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            activeWalls();
            instantiateBoss();
        }
    }

    /// <summary>
    /// Restart the boss zone to the original state without the boss and the walls deactived.
    /// </summary>
    private void restartBossZone()
    {
        activeZone = false;
        Destroy(_boss.gameObject);
        _boss = null;
        deactivateWalls();
   
    }

    /// <summary>
    /// Creates an instance of the gameObject prefab called _bossName.
    /// </summary>
    private void instantiateBoss()
    {
        GameObject _bossgo = null;
        if (_boss == null)
        {
            _bossgo = (GameObject)Instantiate(_bossPrefab, _spawnPoint);
            _boss = _bossgo.GetComponent<Character>();

            //Reset per un bug de Unity
            _boss.GetComponent<Animator>().updateMode = AnimatorUpdateMode.UnscaledTime;
            _boss.GetComponent<Animator>().updateMode = AnimatorUpdateMode.Normal;
        }
    }
}
