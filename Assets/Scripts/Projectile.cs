﻿using UnityEngine;
using System;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]

/// <summary>
/// Class that controls the behaviour of the projectile.
/// </summary>
public abstract class Projectile : MonoBehaviour, Weapon
{
    protected Rigidbody _rb;
    protected float _lifeTimeLong;
    protected float _lifeTimeShort;
    [SerializeField] protected float _shootForce;
    public string _targetTag;
    public string _ownerTag;

    protected bool _hitSomething = false;
    protected bool _hitEnemy = false;

    protected Collider _stickCollider;

    private Collider _myCollider;

    protected float _projectileDmg;

    #region Audio Player
    [SerializeField] protected AudioClip[] _throwClips;
    [SerializeField] protected AudioClip[] _hitClips;
    protected AudioSource _audioSource;

    public bool destroyOnTargetImpact = true;
    private GameObject _audioFont;
    #endregion

    /// <summary>
    /// Initializes values and plays the throw audio. Sets a long timer until the projectile is destroyed.
    /// </summary>
    protected virtual void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
        _myCollider = GetComponent<Collider>();
        playThrowAudio();

        _audioFont = new GameObject();

        destroyProjectile(_lifeTimeLong);
    }

    /// <summary>
    /// The rigidbody of the projectile is frozen and creates a stick effect.
    /// </summary>
    protected virtual void stick(Collision collision)
    {
        _rb.constraints = RigidbodyConstraints.FreezeAll;
        _myCollider.enabled = false;
        //falta amagar renderer

        //gameObject.GetComponent<Renderer>().enabled = false;
        //Joint joint = gameObject.AddComponent<FixedJoint>();
        //joint.connectedBody = collision.rigidbody;
        //Destroy(gameObject);
    }

    /// <summary>
    /// Destroys the projectile in a given time.
    /// </summary>
    protected void destroyProjectile(float time)
    {
        Destroy(gameObject, time);
    }

    /// <summary>
    /// Returns the attack damage
    /// </summary>
    public float getAttackValue()
    {
        return _projectileDmg;
    }

    /// <summary>
    /// Makes damage to the given enemy
    /// </summary>
    protected void makeDamage(Character enemy)
    {
        enemy.takeDamage(getAttackValue());
        _hitEnemy = true;
    }

    /// <summary>
    /// Returns the shootForce
    /// </summary>
    public float getShootForce()
    {
        return _shootForce;
    }

    /// <summary>
    /// Called when hits a surface. If it's not the owner or itself, plays an audio and sets a shorter timer to get destroyed. 
    /// If the collider of the collision has the target tag, makes the collider bleeed, it makes damage to the enemy and also sticks to the target.
    /// If it's the owner, ignores collision.
    /// </summary>
    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != _ownerTag && collision.collider.tag != "Projectile")
        {
            _hitSomething = true;
            //playHitAudio();
            destroyProjectile(_lifeTimeShort);

            //Audio
            GameObject go = Instantiate(_audioFont, transform.position, transform.rotation);
            AudioSource newAudioSource = go.AddComponent<AudioSource>();
            newAudioSource.PlayOneShot(getRandomHitClip());
            Destroy(go, 3);

            print(collision.collider.tag);

            if (collision.collider.tag == _targetTag)
            {
                takeDamageEffect(collision.collider);
                _stickCollider = collision.collider;
                makeDamage(collision.collider.GetComponent<Character>());
                _hitEnemy = true;
                if (destroyOnTargetImpact) destroyProjectile(0.0000000000000000001f);
            }


            //if ((collision.collider.tag == _targetTag || collision.collider.tag == "Projectile") && destroyOnTargetImpact) Destroy(gameObject);
            stick(collision);

        }
        else if (collision.collider.tag == _ownerTag || collision.collider.tag != "Projectile") Physics.IgnoreCollision(collision.collider.GetComponent<Collider>(), this.GetComponent<Collider>());
    }

    /// <summary>
    /// Instantiates a HitBlood effect at the collider position
    /// </summary>
    protected void takeDamageEffect(Collider col)
    {
        GameObject go = Instantiate(Resources.Load<GameObject>("Effects/HitBlood"), col.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position), Quaternion.identity);
        Destroy(go, 1);
    }

    /// <summary>
    /// Returns a random AudioClip of the hitClips
    /// </summary>
    protected AudioClip getRandomHitClip()
    {
        return _hitClips[UnityEngine.Random.Range(0, _hitClips.Length)];
    }

    /// <summary>
    /// Returns a random AudioClip of the throwClips
    /// </summary>
    protected AudioClip getRandomThrowClip()
    {
        return _throwClips[UnityEngine.Random.Range(0, _throwClips.Length)];
    }

    /// <summary>
    /// Plays a random hit Audio in case it has at least one clip in hitClips
    /// </summary>
    protected void playHitAudio()
    {
        try
        {
            AudioClip clip = getRandomHitClip();
            _audioSource.PlayOneShot(clip);
        }
        catch (Exception e)
        {
            Debug.Log("ERROR[Hit Audio Cip]:" + e, this);
        }
    }

    /// <summary>
    /// Plays a random throw Audio in case it has at least one clip in throwClips
    /// </summary>
    protected void playThrowAudio()
    {
        try
        {
            AudioClip clip = getRandomThrowClip();
            _audioSource.PlayOneShot(clip);
        }
        catch (Exception e)
        {
            Debug.Log("ERROR[Throw Audio Cip]:" + e, this);
        }
    }
}
