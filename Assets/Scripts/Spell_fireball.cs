﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A spell fireball is casted from the player toward a direction.
/// </summary>
public class Spell_fireball : Spell
{
    public string _fireballPrefab = "FireballProjectile";
    public float _cooldown = 5f;

    ProjectileController _projectileController;
    public Transform _shotPoint;
    public GameObject _owner;
    public AudioClip _castAudio;
    private int _costSkillPoint;
    private int _level = 0;
    private int _extraDamage = 0;

    //Constructor utilitzat per crear l'spell del que inicialitzarem els clons
    //cridat al shoot, i necessari per el primer instantiate
    /// <summary>
    /// Initializes a Fireball with an owner and a shotpoint
    /// </summary>
    public Spell_fireball(GameObject owner, Transform shotPoint)
    {
        _owner = owner;
        _shotPoint = shotPoint;
        _castAudio = Resources.Load<AudioClip>("sounds/fireball/fireball_cast");
        _projectileController = _owner.GetComponent<ProjectileController>();
        _costSkillPoint = Constants.SPCost_Fireball;
    }

    /// <summary>
    /// Initializes a Fireball with an owner, a shot point and also the name of the fireballPrefab
    /// </summary>
    public Spell_fireball(GameObject player, Transform shotPoint,string fireballPrefab,float cooldown)
    {
        _owner = player;
        _shotPoint = shotPoint;
        _castAudio = Resources.Load<AudioClip>("sounds/fireball/fireball_cast");
        _projectileController = _owner.GetComponent<ProjectileController>();
        _fireballPrefab = fireballPrefab;

        _costSkillPoint = Constants.SPCost_Fireball;
        _cooldown = cooldown;
    }

    /// <summary>
    /// Returns anim layer name
    /// </summary>
    public string getType()
    {
        return "ThrowableSpell";
    }

    /// <summary>
    /// Casts a fireball and stops the owner cast animation
    /// </summary>
    void Spell.cast()
    {
        GameObject dark = Resources.Load<GameObject>(_fireballPrefab);
        GameObject go = _projectileController.shoot(Resources.Load<GameObject> (_fireballPrefab), _shotPoint.position);
        go.GetComponent<SpellBehaviour_fireball>().increaseDamage(_extraDamage);
    }

    /// <summary>
    /// Returns the cast AudioClip
    /// </summary>
    public AudioClip getCastAudio()
    {
        return _castAudio;
    }

    /// <summary>
    /// Returns the name of the spell
    /// </summary>
    public string getName()
    {
        return "Fireball";
    }

    /// <summary>
    /// Returns the cost of the Spell
    /// </summary>
    public int getCostSkillPoint()
    {
        return _costSkillPoint;
    }

    public float cooldown()
    {
        return _cooldown;
    }

    public bool unlocked()
    {
        return _level > 0;
    }

    public void setLevel(int level)
    {
        _level = level;
        setNewLevelBehaviour();
    }

    /// <summary>
    /// Returns the amplified damage by level
    /// </summary>
    public void setNewLevelBehaviour()
    {
        _extraDamage = _level * 4;
    }

}
