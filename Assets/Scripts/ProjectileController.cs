﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that controls projectiles spawn behaviour
/// </summary>
public class ProjectileController : MonoBehaviour
{
    public GameObject _player;
    public Vector3 _offset;
    private GameObject _projectilePrefab; //AssignedPrefab

    /// <summary>
    /// Shoots the assigned prefab from shotPoint
    /// </summary>
    public void shootAssigned(Transform shotPoint)
    {
        if (_projectilePrefab != null)
            shoot(_projectilePrefab, shotPoint);
    }

    /// <summary>
    /// Saves the reference of the projectile prefab to instance
    /// </summary>
    public void setProjPrefab(GameObject projectilePrefab)
    {
        _projectilePrefab = projectilePrefab;
    }

    /// <summary>
    /// Instantiates a projectilePrefab at a shotPoint. And sets the velocity of the instantiated prefab to a shootForce.
    /// </summary>
    public void shoot(GameObject projectilePrefab, Transform shotPoint)
    {
        GameObject go = Instantiate(projectilePrefab, shotPoint.position, Quaternion.identity);
        Rigidbody rb = go.GetComponent<Rigidbody>();
        float shootForce = go.GetComponent<Projectile>().getShootForce();
        rb.velocity = _player.transform.forward * shootForce;
    }

    /// <summary>
    /// Instantiates a projectilePrefab at a shotPoint. And sets the velocity of the instantiated prefab to a shootForce. Also returns the reference to the instantiated prefab.
    /// </summary>
    public GameObject shoot(GameObject projectilePrefab, Vector3 shotPoint)       //hauria de ser aquesta
    {
        GameObject go = Instantiate(projectilePrefab, shotPoint, Quaternion.identity);
        Rigidbody rb = go.GetComponent<Rigidbody>();
        float shootForce = go.GetComponent<Projectile>().getShootForce();
        rb.velocity = _player.transform.forward * shootForce;

        return go;
    }

    //public void shoot(GameObject projectilePrefab, Vector3 shotPoint, Vector3 target)
    //{
    //    GameObject go = Instantiate(projectilePrefab, shotPoint, Quaternion.identity);
    //    Rigidbody rb = go.GetComponent<Rigidbody>();
    //    float shootForce = go.GetComponent<Projectile>().getShootForce();
    //    Vector3 movedirection = (target - transform.position + _offset).normalized  * shootForce; 
    //    rb.velocity = movedirection;
    //}

    /// <summary>
    /// Instantiates a projectilePrefab at a shotPoint. And sets the velocity of the instantiated prefab to a shootForce. Sets the movedirection of the instantiated object towards the target.
    /// Also returns the reference to the instantiated prefab.
    /// </summary>
    public GameObject shoot(GameObject projectilePrefab, Vector3 shotPoint, Vector3 target)
    {
        GameObject go = Instantiate(projectilePrefab, shotPoint, Quaternion.identity);
        Rigidbody rb = go.GetComponent<Rigidbody>();
        float shootForce = go.GetComponent<Projectile>().getShootForce();
        Vector3 movedirection = (target - transform.position + _offset).normalized * shootForce;
        rb.velocity = movedirection;

        return go;
    }

    /// <summary>
    /// Assigns the owner
    /// </summary>
    public void setOwner(GameObject go)
    {
        _player = go;
    }

}
