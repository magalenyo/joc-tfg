﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_IceStorm : Spell
{
    private PlayerController _player;

    private ProjectileController _projectileController;
    private AudioClip _castAudio;

    private int _costSkillPoints;
    [SerializeField] private ushort _baseProjectiles;
    [SerializeField] private ushort _nProjectiles;
    [SerializeField] private float _range;
    [SerializeField] private float _sphereCastRadius;
    [SerializeField] private float _horizontalBoundaries;
    [SerializeField] private float _verticalBoundaries;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _cooldown = 8f;

    private int _level = 0;


    private int _layerMask;

    #region debug   
//    private float _currentHitDistance = 0;
    #endregion

    public Spell_IceStorm(GameObject player)
    {
        _costSkillPoints = Constants.SPCost_IceStorm;
        _baseProjectiles = 4;
        _nProjectiles = 6;
        _range = 8;
        _sphereCastRadius = 0.5f;
        _horizontalBoundaries = 0.5f;
        _verticalBoundaries = 0.3f;
        _offset = new Vector3(0, 3f, 0);

        _layerMask = 1 << 10;

        _player = player.GetComponent<PlayerController>();
        _projectileController = player.GetComponent<ProjectileController>();

        _castAudio = Resources.Load<AudioClip>("sounds/icestorm/icestorm_cast2");
    }

    //void Start()
    //{
    //    _costSkillPoints = 4;
    //    _nProjectiles = 6;
    //    _range = 8;
    //    _sphereCastRadius = 0.5f;
    //    _horizontalBoundaries = 0.5f;
    //    _verticalBoundaries = 0.1f;
    //    _offset = new Vector3(0, 3f, 0);

    //    _layerMask = 1 << 10;

    //    _player = PlayerController.instance.GetComponent<PlayerController>();
    //    _projectileController = _player.GetComponent<ProjectileController>();
    //}

    public void cast()
    {

        Vector3 spawnPosition = getPosition();
        for (ushort i = 0; i < _nProjectiles; i++)
        {
            Vector3 shotPoint = spawnPosition + _offset + new Vector3(UnityEngine.Random.Range(-_horizontalBoundaries, _horizontalBoundaries),
                                                                      UnityEngine.Random.Range(-_verticalBoundaries, _verticalBoundaries),
                                                                      0);
            _projectileController.shoot(Resources.Load<GameObject>("IceStormProjectile"), shotPoint);
        }
        //_player.endCasting();
    }

    private Vector3 getPosition()
    {
        RaycastHit hit;
        if (Physics.SphereCast(_player.transform.position + _player._controller.center, _sphereCastRadius, _player.transform.forward, out hit, _range, _layerMask, QueryTriggerInteraction.Ignore))
        {
            //_currentHitDistance = hit.distance;
            return hit.point;
        }
        return _player.transform.position + _player._controller.center + _player.transform.forward * _range;

    }

    public AudioClip getCastAudio()
    {
        return _castAudio;
    }

    public int getCostSkillPoint()
    {
        return _costSkillPoints;
    }

    public string getName()
    {
        return "Ice Storm";
    }

    public string getType()
    {
        return "CastableSpell";
    }

    public float cooldown()
    {
        return _cooldown;
    }

    public bool unlocked()
    {
        return _level > 0;
    }

    public void setLevel(int level)
    {
        _level = level;
        setNewLevelBehaviour();
    }

    public void setNewLevelBehaviour()
    {
        _nProjectiles = (ushort) (_baseProjectiles + _level*2);
    }

    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Debug.DrawLine(_player.transform.position + _player._controller.center, _player.transform.position + _player._controller.center + _player.transform.forward * _range);
    //    Gizmos.DrawWireSphere(_player.transform.position + _player._controller.center + _player.transform.forward * _range, _sphereCastRadius);
    //}

}
