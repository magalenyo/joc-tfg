﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventHandler : MonoBehaviour
{
    public delegate void PlayerHealthEventHandler(float currentHealth, float maxHealth);
    public static event PlayerHealthEventHandler onPlayerHealthChanged;

    public delegate void PlayerExperienceHandler(float experience, float maxExperience, int level);
    public static event PlayerExperienceHandler onExperienceGained;

    public delegate void PlayerLevelhandler();
    public static event PlayerLevelhandler onLevelUp;

    public delegate void PlayerSoulHandler(int souls);
    public static event PlayerSoulHandler onSoulCollected;

    public delegate void PlayerSkillPointHandler();
    public static event PlayerSkillPointHandler onSkillPointUsed;

    public delegate void PlayerEquipmentHandler(string equipmentRsc, int counter);
    public static event PlayerEquipmentHandler onEquipmentChanged;

    public delegate void PlayerGUIDisableHandler();
    public static event PlayerGUIDisableHandler onBasicGUIDisabled;

    public delegate void PlayerGUIEnableHandler();
    public static event PlayerGUIEnableHandler onBasicGUIEnabled;

    public delegate void PlayerDeadHandler();
    public static event PlayerDeadHandler onDead;


    public static void healthChanged(float currentHealth, float maxHealth)
    {
        onPlayerHealthChanged(currentHealth, maxHealth);
    }

    public static void experienceGained(float experience, float maxExperience, int level)
    {
        onExperienceGained(experience,maxExperience,level);
    }

    public static void levelUp()
    {
        onLevelUp();
    }

    public static void soulCollected(int souls)
    {
        onSoulCollected(souls);
    }

    public static void useSkillPoint()
    {
        onSkillPointUsed();
    }

    public static void changeEquipment(string equipmentRsc, int counter)
    {
        onEquipmentChanged(equipmentRsc, counter);
    }

    public static void disableBasicGUI()
    {
        onBasicGUIDisabled();
    }

    public static void enableBasicGUI()
    {
        onBasicGUIEnabled();
    }

    public static void die()
    {
        onDead();
    }

}
