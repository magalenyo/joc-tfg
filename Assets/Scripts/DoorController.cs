﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
   
    private Vector3 _OnOpenPosition;
    private Vector3 _originalPosition;
    public float _incremental =  -3f;
    private float _targetValue;
    private bool goUp = false;

    private bool move;


    private void Start()
    {
        if (goUp) _incremental = _incremental * -1;
        _targetValue = _incremental;
        _originalPosition = transform.position;
        move = false;
       
    }

    public void changeStat()
    {
        if (_targetValue == 0) _OnOpenPosition = _originalPosition;
        else   _OnOpenPosition = new Vector3(_originalPosition.x, _originalPosition.y + _targetValue, _originalPosition.z);

        nextTarget(); 
        move = true;
    }

    private void Update()
    {
       if (move)
        {
            transform.position = Vector3.Lerp(transform.position, _OnOpenPosition, 1 * Time.deltaTime); //ha de tenir un smooth major (10f)
            if (transform.position == _OnOpenPosition) move = false;
        }
    }

    private void nextTarget()
    {
        if (_targetValue == 0)
        {
            _targetValue = _incremental;
        }
        else
            _targetValue = 0;
    }
}
