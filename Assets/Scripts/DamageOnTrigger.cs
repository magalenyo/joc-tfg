﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent (typeof (Collider))]
[RequireComponent(typeof(AudioSource))]
public class DamageOnTrigger : MonoBehaviour
{

    public bool instakill = false;
    public float damage = 10f;
    public AudioClip _audioOnHit; //Optional Sound on Hit


    /// <summary>
    ///When the player collides it kills him.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (instakill)
        {
            damage = int.MaxValue;
        }
        if (other.tag == Constants.Tag_Enemy || other.tag == Constants.Tag_Player)
        {
          
            other.GetComponent<Character>().takeDamage(damage);

            if (_audioOnHit != null)
            {
                GetComponent<AudioSource>().PlayOneShot(_audioOnHit);
            }
        }
    }
}
