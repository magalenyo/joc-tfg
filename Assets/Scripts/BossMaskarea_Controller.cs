﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

/// <summary>
/// Controls the behaviour of the Boss Maskarea
/// </summary>
public class BossMaskarea_Controller : Enemy
{
    private Transform _target;
    private NavMeshAgent _agent;
    [SerializeField] private float _distanceToTarget;
    [SerializeField] private float _lookRadius;
    private CapsuleCollider _myCollider;

    private System.Random randomMeleeAttack;
    private int _currentMeleeAttack;

    //CoolDowns
    private float timeToMeleeAgain;
    public float Melee_coolDownInSeconds = 2;
    public float Spell_coolDownInSeconds = 10;

    //Phase 2 Buffs
    [Header("Fase 2 BUFFs")]
    [SerializeField] private float _percentHealthPhase2 = 0.4f;
    [SerializeField] private float _melee_coolDownInSecondsPhase2 = 0.5f;
    [SerializeField] private float _attackSpeedMultPhase2 = 2f;
    [SerializeField] private float _extraDamagePhase2 = 10f;

    //Weapons
    [SerializeField] private MeleeWeaponController _boot;
    [SerializeField] private MeleeWeaponController _sword;
   
    private MeleeAttackList _meleeAttackList;
    
    //Spells
    private SpellController _spellController;
    private ProjectileController _projectileController;
    [SerializeField] private Transform _shotPoint;

    //Phase 
    private bool _Phase2;
    private bool _onFloor;
    private float _healthTrigger;
    [SerializeField] private GameObject _effects;

    #region ExtraActions
    private string _isFalling = "fall";
    private string _isCasting = "isCasting";
    private string _attackSpeed = "attackSpeed";

    #endregion

    private void Awake()
    {
        initializeStats();
        initializeSpells();
    }

    protected override void Start()
    {
        
        base.Start();//Call the original character's method
        setExtraActions();
        randomMeleeAttack = new System.Random();
        initializeStats();

        //Combo Ini
        _meleeAttackList = new MeleeAttackList();
        initializeMeleeCombos();

        timeToMeleeAgain = Time.time + 0;
       
        //Phase 2
        _Phase2 = false;
        _onFloor = false;
        _healthTrigger = _stats.getStat(Constants.maxHealth) * _percentHealthPhase2;

        //Agent start
        _target = PlayerController.instance.getPosition();
        _agent = GetComponent<NavMeshAgent>();
        _myCollider = GetComponent<CapsuleCollider>();

        _animFloats[_attackSpeed] = 1f;
        _effects.SetActive(false);

    }
    
    void Update()
    {

        if (canStartPhase2())
        {
            //Phase 2 checker
            _actions[_isFalling] = true;
            _myCollider.enabled = false;
        }

        _distanceToTarget = Vector3.Distance(_target.position, transform.position); //Actualitza la distancia fins al jugador.
        move();//S'ocupa de posar a true i a fals el moviment.

        if (!_actions[_isFalling] && !isDead() && !isPlayerDead()) //Comprovem que no esta passan de fase ni morint.
        {
            if (!isMakingAttack())//Check no esta fent un atac de cap tipus.
            {
               
                faceTarget();//Sempre apunta al jugador a no ser qu estigui executant un combo.

                //Select attack type
                if (_distanceToTarget >= 4 && _distanceToTarget <= 10 && _spellController.canCastCurrent())
                {
                    castSpell();
                }
                else if (stopDistance())
                {
                    meleeAttack();
                }
         
            }
        }

        ///*** ACCIONS DE TRANSICIÓ DE FASE **///
        if (_actions[_isFalling])//Attack prevoius to start fase 2.
        {
            if (_distanceToTarget <= 2.5f)
            {//when player is in a short range
                
                if (_onFloor)
                {
                    faceTarget();
                    _actions[_attacking] = true;
                }
                    
            }
        }

        //Update Animations
        _animController.updateAnimations(_actions, _animLayer, _animFloats);

    }

    private void FixedUpdate()
    {
        if (transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        }
    }

    /// <summary>
    /// Returns true if the boss is making some kind of action considered attack.
    /// </summary>
    private bool isMakingAttack()
    {
        return _actions[_isCasting] || _actions[_attacking];
    }

    /// <summary>
    /// Returns true if the boss is making an action that restrict the movement.
    /// </summary>
    bool isMakingRestAction()
    {
        return isMakingAttack() || _actions[_isFalling] || _actions[_isDying];
    }

    /// <summary>
    /// Event called from animation. Sets the boss onFloor to true.
    /// </summary>
    public void event_onFloor()
    {
        _onFloor = true;
    }

    #region Spells

    /// <summary>
    /// Sets the casting action to true.
    /// </summary>
    void castSpell()
    {
        _actions[_isCasting] = true;
    }

    /// <summary>
    /// Event called from animation. Cast the spell making the effect.
    /// </summary>
    public void event_useSpell()
    {
        _spellController.castCurrent();
    }

    /// <summary>
    /// Event called from animations. Sets the casting action to false.
    /// </summary>
    public void event_endCasting()
    {
        _actions[_isCasting] = false;
    }

    #endregion

    #region meleeAttack

    private void meleeAttack()
    {
      
        if (timeToMeleeAgain <= Time.time)//Cooldown to do the next move
        {
            _actions[_attacking] = true;
            _currentMeleeAttack = getMeleeAttack();
            _actions[_meleeAttackList.getCombo(_currentMeleeAttack)] = true;
        }
        
    }

    private int getMeleeAttack()
    {
        return randomMeleeAttack.Next(0, _meleeAttackList.size());
       
    }
    #endregion

    #region mesurement

    private bool stopDistance()
    {
        return _distanceToTarget <= _agent.stoppingDistance;
    }

    #endregion

    #region Movement
    /// <summary>
    /// Controls the boss move, sets to true or false the action of moving.
    /// </summary>
    private void move()
    {

        if (!stopDistance() && !isMakingRestAction()) 
        {
            _agent.SetDestination(_target.position);
            _actions[_moving] = true;
            _agent.isStopped = false;
        }

        else
        {
            _agent.isStopped = true;
            _actions[_moving] = false;
        }

        _animFloats[_speed] = Math.Abs(_agent.speed);
    }

    /// <summary>
    /// Makes the boss look at the player.
    /// </summary>
    void faceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 50f);
    }

    #endregion

    #region MeleeAttack Events
    //Called when the attack animation is ended
    public void event_endAttack()
    {
        timeToMeleeAgain = Time.time + Melee_coolDownInSeconds;

        _actions[_attacking] = false;
        _actions[_meleeAttackList.getCombo(_currentMeleeAttack)] = false;
    }

 
 
    //Between the following 2 events the weapon used to do the combo makes damage.
    public void event_swoosh()
    {
     
        _meleeAttackList.get(_currentMeleeAttack).Item2.attack();
    }

    //Used to do damage again when is using a combo.
    public void event_resetAttack()
    {
        _meleeAttackList.get(_currentMeleeAttack).Item2.stopAttack();
    }
    #endregion

    #region Fase transition
    private bool canStartPhase2()
    {
        return (!_Phase2 && (_stats.getStat(Constants.currentHealth) <= _healthTrigger));
   
    }

    private void event_startNewFase()
    {
        //Buffs
        Melee_coolDownInSeconds = _melee_coolDownInSecondsPhase2; //Cooldwon reduccion
        _animFloats[_attackSpeed] = _attackSpeedMultPhase2; //Double attack speed;
        _stats.increaseStat(Constants.strength, _extraDamagePhase2);//More damage;
        _spellController.getCurrentSpell().setLevel(3); //2 extra levels 

        _onFloor = false;
        _Phase2 = true;


        _myCollider.enabled = true;
        _actions[_attacking] = false;
        _actions[_isFalling] = false;

    }
    public void turnEffectsOn()
    {
        _effects.SetActive(true);
    }
    public void turnEffectsOff()
    {
        _effects.SetActive(false);
    }
    #endregion

    #region Initialization

    /// <summary>
    /// Set all the boss stats.
    /// </summary>
    protected override void initializeStats()
    {

        Dictionary<string, float> stats = new Dictionary<string, float>();
        stats.Add(Constants.maxHealth, Constants.Stats_BossMaskarea_maxHealth);
        stats.Add(Constants.currentHealth, Constants.Stats_BossMaskarea_maxHealth);
        stats.Add(Constants.resMagic, Constants.Stats_BossMaskarea_resMagic);
        stats.Add(Constants.resPhys, Constants.Stats_BossMaskarea_resPhys);
        stats.Add(Constants.moveSpeed, Constants.Stats_BossMaskarea_moveSpeed);
        stats.Add(Constants.cooldown, Constants.Stats_BossMaskarea_cooldown);
        stats.Add(Constants.strength, Constants.Stats_BossMaskarea_strength);
        stats.Add(Constants.magic, Constants.Stats_BossMaskarea_magic);

        stats.Add(Constants.experience, Constants.Stats_BossMaskarea_experience);
        stats.Add(Constants.soulCounter, Constants.Stats_BossMaskarea_souls);
        _stats = new CharacterStats(stats);
    }

    /// <summary>
    /// Set the unique actions not inherited.
    /// </summary>
    protected override void setExtraActions()
    {
        _actions[_isFalling] = false;
        _actions[_isCasting] = false;
        _actions["mc1"] = false;
        _actions["mc2"] = false;
        _actions["mc3"] = false;
        _actions["mc4"] = false;
    }

    /// <summary>
    /// Adds the melee combos to meleeAttackList.
    /// </summary>
    private void initializeMeleeCombos()
    {
        _meleeAttackList.add("mc1", _sword);
        _meleeAttackList.add("mc2", _sword);
        _meleeAttackList.add("mc3", _sword);
        _meleeAttackList.add("mc4", _boot);

    }

    /// <summary>
    /// Initialize the spells that can be casted.
    /// </summary>
    private void initializeSpells()
    {

        Spell Darkfireball = new Spell_fireball(gameObject, _shotPoint, "DarkPowerProjectile", Spell_coolDownInSeconds);
        Darkfireball.setLevel(1);

        List<Spell> allSpells = new List<Spell>();
        allSpells.Add(Darkfireball);
   
        _spellController = new SpellController(allSpells);
        _spellController.assignSpell(_spellController.Primary, 0);

        //Nomes te un spell no canvia.
        _spellController._current = _spellController.Primary;
    }

    #endregion

    #region Debug
    //Debug, dibuixem l'area d'awake
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, _lookRadius);
    }
    #endregion

    public override void teleportTo(Vector3 _position)
    {
        _agent.Warp(_position);
    }

}
