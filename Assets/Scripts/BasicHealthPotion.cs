﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicHealthPotion : Consumible
{
    [SerializeField]
    private int _health = 10;

    public override void consume(Consumer _consumer)
    {
        _consumer.increaseHealth(_health);
        _consumer.drink();
    }
}
