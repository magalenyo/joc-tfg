﻿using System;

public static class ExperienceSystem
{
    public static float _playerExperience = 0;
    public static int _playerLevel = 0;
    public static float _requiredExperience = 1;  //for next level
    public static float _skillPoints = 0;

    //private void Start()
    //{
    //    SystemHandler.onExperienceGained += addExperience;
    //    SystemHandler.tryLevelUp += levelUp;
    //    SystemHandler.levelUp += levelUp;
    //}

    /// <summary>
    /// xp added to player's current experience
    /// </summary>
    public static void addExperience(float xp)
    {
        _playerExperience += xp;
    }

    /// <summary>
    /// returns true if player's experience is equal or greater to the required experience
    /// </summary>
    private static bool canLevelUp()
    {
        return _playerExperience >= _requiredExperience;
    }

    /// <summary>
    /// Resets player's experience, sets next level required experience, increases player's level in 1, adds skill points in relation with player's level
    /// </summary>
    public static void levelUp()
    {
        bool leveledUp = false;
        while(canLevelUp())
        {
            leveledUp = true;
            resetExperience();
            setNextLevel();
            increaseLevel();
            addSkillPoints();
        }
        if (leveledUp) UIEventHandler.levelUp();
    }

    /// <summary>
    /// Resets player experience to the current player's experience - required experience, so if the player has 105xp and the required is 100, 5 will be stored
    /// </summary>
    private static void resetExperience()
    {
        _playerExperience -= _requiredExperience;
    }

    /// <summary>
    /// Multiplies 5x required experience 
    /// </summary>
    private static void setNextLevel()
    {
        _requiredExperience *= 2;
    }

    /// <summary>
    /// PlayerLevel++
    /// </summary>
    private static void increaseLevel()
    {
        _playerLevel++;
    }

    /// <summary>
    /// Adds skill points based on level
    /// </summary>
    private static void addSkillPoints()
    {
        _skillPoints += (float) Math.Ceiling(_playerLevel * 0.4f);
    }

    /// <summary>
    /// Returns true if the skill points counter > 0
    /// </summary>
    public static bool canUseSkillPoints()
    {
        return _skillPoints > 0;
    }

    /// <summary>
    /// Returns true if the skill points counter >= cost
    /// </summary>
    public static bool canUpgradeSkillWithCost(int cost)
    {
        return _skillPoints >= cost;
    }

    /// <summary>
    /// Skill points - amount
    /// </summary>
    public static void decreaseSkillPoint(int amount)
    {
        _skillPoints -= amount;
    }

    /// <summary>
    /// Skill points + amount
    /// </summary>
    public static void increaseSkillPoint(int amount)
    {
        _skillPoints += amount;
    }

    public static void initializeExperience()
    {
        _playerExperience = 0;
        _playerLevel = 0;
        _requiredExperience = 1;  //for next level
        _skillPoints = 0;
    }
}
