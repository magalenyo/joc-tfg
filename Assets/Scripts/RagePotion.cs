﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Temporal Stats Potion increases players strenght.
/// </summary>
public class RagePotion : TempStatsPotion
{
    /// <summary>
    /// Increases consumer's strength.
    /// </summary>
    private void Start()
    {
        _Stat = Constants.strength;
    }
}
