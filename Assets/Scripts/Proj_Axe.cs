﻿using UnityEngine;

/// <summary>
/// Projectile Axe Controller
/// </summary>
public class Proj_Axe : Projectile
{
    public float turnspeed;
    private Vector3 _directionSpawn;
    private Vector3 _directionRotate;
    /// <summary>
    /// Initializes values
    /// </summary>
    public Proj_Axe()
    {  
        _lifeTimeLong = Constants.Lt_Long_ThrowableAxe;
        _lifeTimeShort = Constants.Lt_Short_ThrowableAxe;
        _projectileDmg = Constants.Dmg_ThrowableAxe;
        _shootForce = Constants.Speed_ThrowableAxe;
    }

    /// <summary>
    /// Makes the axe spin towards a direction.
    /// </summary>
    protected override void Start()
    {
        //_rb = GetComponent<Rigidbody>();
        //transform.rotation = Quaternion.LookRotation(_rb.velocity * -1);
        //_directionSpawn = _rb.velocity.x <= 0 ? Vector3.up : Vector3.down;
        //transform.rotation = Quaternion.LookRotation(_directionSpawn);
        //_directionRotate = Vector3.down;
        base.Start();
        transform.rotation = Quaternion.LookRotation(_rb.velocity * -1);
        _directionSpawn = _rb.velocity.x <= 0 ? Vector3.up : Vector3.down;
        transform.rotation = Quaternion.LookRotation(_directionSpawn);

        
        _directionRotate = Vector3.down;

        
    }

    /// <summary>
    /// Spins the axe until it hits something.
    /// </summary>
    void Update()
    {
        if (!_hitSomething){
            transform.Rotate(_directionRotate,  turnspeed * Time.deltaTime);
        }

        if (_hitEnemy && _stickCollider == null) Destroy(gameObject);
    }

}
