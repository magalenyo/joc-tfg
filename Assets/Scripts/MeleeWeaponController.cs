﻿using UnityEngine;
using System;
/// <summary>
/// Melee weapon controller, current users; player, melee_Enemy1
/// </summary>
/// 
[RequireComponent(typeof(AudioSource))]

/// <summary>
/// Controls a Weapon
/// </summary>
public class MeleeWeaponController : MonoBehaviour, Weapon
{
    public GameObject _characterObject;     //Weapon's owner (Player, Enemy)
    public float _weapon_dmg;               //Weapon's damage
    public string _targetTag;               //Tag of who must affect

    private Character _character;           //Owner's character
    private Collider _collider;             //Weapon's collider
    private float _character_dmg = -1;           //Owner's damage

    private bool _alreadyAttacked;          //not repeat the dmg

    #region Audio Player
    [SerializeField] protected AudioClip[] _swooshClips;
    [SerializeField] protected AudioClip[] _hitClips;
    protected AudioSource _audioSource;
    #endregion

    /// <summary>
    /// Initializes values and components
    /// </summary>
    void Start()
    {
        _character = _characterObject.GetComponent<Character>();
        //_character = GetComponent<Character>();
        try
        {
            _character_dmg = _character.getStat(Constants.strength);
        }
        catch { }
       
        _collider = GetComponent<Collider>();
        if (_collider == null)
            print("ERROR::: NULL COLLIDER" + gameObject);
        _audioSource = GetComponent<AudioSource>();

        _collider.enabled = false;
        _alreadyAttacked = false;
    }

    /// <summary>
    /// When collides with the target and has not already attacked, it makes it damage and plays audio
    /// </summary>
    private void OnTriggerEnter(Collider col)
    {
        if(_character_dmg == -1)
        {
            try
            {
                _character_dmg = _character.getStat(Constants.strength);
            }
            catch { }
        }

        if (col.tag == _targetTag && !_alreadyAttacked)
        {
            playAudio(_hitClips);
            takeDamageEffect(col);
            col.GetComponent<Character>().takeDamage(_weapon_dmg + _character_dmg);

            _alreadyAttacked = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void takeDamageEffect(Collider col)
    {
        GameObject go = Instantiate(Resources.Load<GameObject>("Effects/HitBlood"), col.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position), Quaternion.identity);
        Destroy(go, 1);
    }

    /// <summary>
    /// Returns weapon's damage value
    /// </summary>
    float Weapon.getAttackValue()
    {
        return (_weapon_dmg + _character_dmg);
    }


    /// <summary>
    /// Sets the weapon's collider to enable
    /// </summary>
    //public void borrar()
    //{
    //    _collider.enabled = false;
    //}

    /// <summary>
    /// Sets the weapon's collider to disable
    /// </summary>
    public void stopAttack()
    {
      
        if (_collider == null) print("ERROR::: NULL COLLIDER" + gameObject);

        _collider.enabled = false;
        _alreadyAttacked = false;
    }


    /// <summary>
    /// Enables weapon's collider
    /// </summary>
    public void attack()
    {
       
        _collider.enabled = true;
        playAudio(_swooshClips);
    }

    /// <summary>
    /// Returns an AudioClip, generated randomly from the audioClips list
    /// </summary>
    protected AudioClip getRandomHitClip(AudioClip[] audioClips)
    {
        return audioClips[UnityEngine.Random.Range(0, audioClips.Length)];
    }


    /// <summary>
    /// Plays a random hit AudioClip in case it has at least one
    /// </summary>
    protected void playAudio(AudioClip[] audioClips)
    {
        try
        {
            AudioClip clip = getRandomHitClip(audioClips);
            _audioSource.PlayOneShot(clip);
        }
        catch (Exception e)
        {
            //Debug.Log("ERROR[Hit Audio Cip]:" + e, this);
        }
    }

}
