﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Interface all spell must implement (not behaviours)
/// </summary>
public interface Spell
{
    /// <summary>
    /// Casts the spell
    /// </summary>
    void cast();
    /// <summary>
    /// Returns the spell animation layer name
    /// </summary>
    string getType();
    /// <summary>
    /// Returns the name of the spell to display
    /// </summary>
    string getName();
    /// <summary>
    /// Returns the cost of the spell in skill points in order to purchase
    /// </summary>
    int getCostSkillPoint();
    /// <summary>
    /// Returns the casting AudioClip
    /// </summary>
    AudioClip getCastAudio();
    /// <summary>
    /// Returns the cooldown in seconds.
    /// </summary
    float cooldown();
    /// <summary>
    /// Returns true if level > 0
    /// </summary>
    bool unlocked();
    /// <summary>
    /// Sets the spell level to the param level
    /// </summary>
    void setLevel(int level);
    /// <summary>
    /// Sets the new level behaviour
    /// </summary>
    void setNewLevelBehaviour();

    //faltaria un can be cast de unlocked per exemple, o a spell controller ja veurem
}
