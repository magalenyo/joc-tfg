﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the behaviour of the fireball
/// </summary>
public class SpellBehaviour_fireball : Projectile
{
    /// <summary>
    /// Initializes values
    /// </summary>
    public SpellBehaviour_fireball()
    {
        _lifeTimeLong = Constants.Lt_Long_Fireball;
        _lifeTimeShort = Constants.Lt_Short_FireBall;
        _projectileDmg = Constants.Dmg_Fireball;
        //_shootForce = Constants.Speed_Fireball;
        _shootForce = 1;
    }

    /// <summary>
    /// Overrides inherited method, stick won't have any effect since wants to be destroyed on hit.
    /// </summary>
    protected override void stick(Collision collision) { }

    /// <summary>
    /// Tabs the game, which means it enables the Tab Menu Views, disables the Basic GUI and also freezes the game (timeScale = 0)
    /// </summary>
    protected override void Start()
    {
        base.Start();
        _rb = GetComponent<Rigidbody>();
        if (_rb.velocity.x < 0) transform.Rotate(0, 180, 0);
        _rb.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        destroyProjectile(_lifeTimeLong);
        
    }

    void Update()
    {
        //if (_hitEnemy && _stickCollider == null) Destroy(gameObject);
    }

    /// <summary>
    /// Increases damage by a value
    /// </summary>
    public void increaseDamage(float value)
    {
        _projectileDmg += value;
    }

}
