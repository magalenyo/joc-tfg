﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FlyingRangedEnemy : Enemy
{
    [Header("Agent variables")]

    public PlayerController _player;
    public Transform _target;
    public NavMeshAgent _agent;

    public float _distanceToTarget;
    public float _distanceToCeiling;
    public float _lookRadius;
    public float _floatingHeight;
    public float _offsetRepositionValue;

    private float _initialBaseOffset = 1.5f;
    private float _tolerableError = 0.1f;

    [Header("Flying Ranged Enemy variables")]
    [SerializeField] private float _movementSpeed = 40f;
    [SerializeField] private float _cooldown = 3f;
    private ProjectileController _projectileController;
    private bool _canAttack = true;

    public Transform _shotPoint;

    public AudioClip _throwAudio;

    /// <summary>
    /// Returns true if it hits something above himself
    /// </summary>
    private bool _mustVerticallyReposition
    {
        get
        {
            return Physics.Raycast(transform.position, Vector3.up, _distanceToCeiling);
        }
    }

    /// <summary>
    /// Returns true if it's vertically aligned with the player
    /// </summary>
    private bool _verticallyRepositionedAbove
    {
        get
        {
            //return _agent.baseOffset >= _initialBaseOffset;
            return _agent.baseOffset >= (_player.getPlayerCurrentCenter() + _floatingHeight + _tolerableError);
        }
    }

    private bool _verticallyRepositionedBelow
    {
        get
        {
            //return _agent.baseOffset >= _initialBaseOffset;
            //return _agent.baseOffset >= (_player.getPlayerCurrentCenter() + _floatingHeight - _tolerableError)
            return _agent.baseOffset <= (_player.getPlayerCurrentCenter() + _floatingHeight - _tolerableError);
        }
    }

    private void Awake()
    {
        initializeStats();

    }

    protected override void Start()
    {
        base.Start();
        //initializeStats();

        _player = PlayerController.instance;
        _target = PlayerController.instance.transform;
        _agent = GetComponent<NavMeshAgent>();
        _projectileController = GetComponent<ProjectileController>();

        //if (_agent.baseOffset != 0) _initialBaseOffset = _agent.baseOffset; 
        //else _agent.baseOffset = _initialBaseOffset;

        //_initialBaseOffset = _target.position.y + _floatingHeight;
        _agent.baseOffset = _player.getPlayerCurrentCenter() + _floatingHeight;

        setFootstepSounds("flyer/FlyerStep");

    }

    void Update()
    {
        _distanceToTarget = Vector3.Distance(_target.position, transform.position);
        if (!isDead() && !isPlayerDead())
        {
            if(!_actions[_attacking]) move();
            faceTarget();
            attack();
        }
        else
        {
            _rb.useGravity = true;
        }

        updateAnimations();
    }

    private void FixedUpdate()
    {
        if (!isDead() && transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);

        }
    }

    private void move()
    {
        if (_distanceToTarget <= _lookRadius && _distanceToTarget > _agent.stoppingDistance)
        {
            _agent.SetDestination(_target.position);
            _actions[_moving] = true;
            _agent.isStopped = false;
            if (!_mustVerticallyReposition)
            {
                if (_verticallyRepositionedAbove)
                {
                    verticallyRepositionDescend();
                }
                else if (_verticallyRepositionedBelow)
                {
                    verticallyRepositionAscend();
                }
            }
            else
            {
                verticallyRepositionDescend();
            }
        }
        else
        {
            _actions[_moving] = false;
        }
    }


    private void attack()
    {
        if (_distanceToTarget <= _lookRadius && _distanceToTarget <= _agent.stoppingDistance && !_actions[_attacking] && _canAttack)
        {
            _canAttack = false;
            _actions[_attacking] = true;
            _actions[_moving] = false;


            StartCoroutine(refreshCooldown());
        }

    }

    IEnumerator refreshCooldown()
    {
        yield return new WaitForSeconds(_cooldown);
        _canAttack = true;
    }

    private void verticallyRepositionDescend()
    {
        if (_agent.baseOffset - _offsetRepositionValue >= _player.getPlayerCurrentCenter())
        {
            _agent.baseOffset -= _offsetRepositionValue;
        }
    }

    private void verticallyRepositionAscend()
    {
        //if (_agent.baseOffset < _initialBaseOffset )
        //{
            _agent.baseOffset += _offsetRepositionValue;
        //}
    }


    private void faceTarget()
    {
        if (_distanceToTarget <= _lookRadius)
        {
            Vector3 direction = (_target.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }
    }

    public override void teleportTo(Vector3 _position)
    {
        _agent.Warp(_position);
    }

    protected override void initializeStats()
    {
        Dictionary<string, float> stats = new Dictionary<string, float>();
        stats.Add(Constants.maxHealth, Constants.Stats_FlyingRanged_maxHelth);
        stats.Add(Constants.currentHealth, Constants.Stats_FlyingRanged_maxHelth);
        stats.Add(Constants.resMagic, Constants.Stats_FlyingRanged_resMagic);
        stats.Add(Constants.resPhys, Constants.Stats_FlyingRanged_resPhys);
        stats.Add(Constants.moveSpeed, Constants.Stats_FlyingRanged_moveSpeed);
        stats.Add(Constants.strength, Constants.Stats_FlyingRanged_strength);
        stats.Add(Constants.experience, Constants.Stats_FlyingRanged_experience);
        stats.Add(Constants.soulCounter, Constants.Stats_FlyingRanged_souls);
        stats.Add(Constants.magic, 0);
        stats.Add(Constants.cooldown, 0);

        _stats = new CharacterStats(stats);
    }

    protected override void setExtraActions()
    {
       
    }

    public void event_attack()
    {
        _projectileController.shoot(Resources.Load<GameObject>("FlyProjectile"), _shotPoint.position, _target.position);
        playThrowAudio();

    }

    public void playThrowAudio()
    {
        _audioSource.PlayOneShot(_throwAudio);
    }

    public void event_endAttack()
    {
        _actions[_attacking] = false;
    }


    //protected override void die()
    //{
    //    base.die();
    //}
    //void OnDrawGizmosSelected()
    //{
    //        Gizmos.color = Color.blue;
    //        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x,transform.position.y+_distanceToCeiling,transform.position.z));

    //}
}
