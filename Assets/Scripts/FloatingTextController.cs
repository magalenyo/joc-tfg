﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatingTextController : MonoBehaviour
{
    [TextArea] public string _textToShow = "Press F to interact";
    public float _textSize = 20f;
    public float _destroyTime = 8f;
    private bool _alreadyInstantiated = false;
    public Transform _instancePoint;

    // Start is called before the first frame update
    void Start()
    {
    }

    /// <summary>
    /// If the text activator is triggered by the player and not alreadyInstantiated, instantiates a floating text at the instancePoint
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == Constants.Tag_Player && !_alreadyInstantiated)
        {
            _alreadyInstantiated = true;
            GameObject go = Instantiate(Resources.Load<GameObject>("UI/FloatingText"), _instancePoint);
            TextMeshPro tm = go.GetComponent<TextMeshPro>();
            tm.text = _textToShow;
            tm.fontSize = _textSize;
            StartCoroutine(destroyText(go));
        }
    }

    /// <summary>
    /// Creates text gameObject, sets new text, and destroys it in _destroyTime
    /// </summary>
    /// <returns></returns>
    IEnumerator destroyText(GameObject go)
    {
        yield return new WaitForSeconds(_destroyTime);
        Destroy(go);
        _alreadyInstantiated = false;
    }

}
