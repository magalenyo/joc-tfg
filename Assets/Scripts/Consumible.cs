﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumible : MonoBehaviour
{
    public abstract void consume(Consumer _consumer);  
}

