﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioSettings : MonoBehaviour
{
    public AudioMixer _audioMixer;
    private Slider _slider;

    private void Start()
    {
        _slider = GetComponent<Slider>();
        float value = 0;
        bool correct = _audioMixer.GetFloat("Volume", out value);
        if (correct) _slider.value = value;
    }

    public void setVolume(float volume)
    {
        _audioMixer.SetFloat("Volume", volume);
    }
}
