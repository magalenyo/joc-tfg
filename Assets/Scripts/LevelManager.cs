﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    
    public static LevelManager instance;
    public bool _spawnAllEnemies = true;        //If is true all enemies will spawn at leve's start
    public bool _disableOldSpawns = false;      //Is is true disable checkpoints when a new current checkpoint is assigned
    public bool _easyMode = false;              //If is true you dont need souls to respawn on the last checkpoint.
    public int _level = 1;                      //Sets the level value (difficult)

    public PlayerController _playerController;
    public List<RespawnController> _respawnList; //All spawns of the level
    public RespawnController _spawnPoint;        //Current spawn of the player
    public RespawnController _firstLevelSpawn;

    #region Menus
    [SerializeField] private DeadMenu _deadMenu;
    #endregion

    private void Awake()
    {
        if (instance != null) Destroy(instance);
        instance = this;
        _respawnList = new List<RespawnController>();
      
    }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        PlayerController.OnPlayerDead += tryAgain;//Subscribe
        _playerController = PlayerController.instance;
        _deadMenu = PlayerPanel.instance._deadMenuUI.GetComponent<DeadMenu>();
        if (_firstLevelSpawn == null) _playerController.teleportTo(StaticResources.getLevelPosition(_level));
        else _playerController.teleportTo(_firstLevelSpawn.transform.position);

    }
    private void OnDestroy()
    {
        PlayerController.OnPlayerDead -= tryAgain;//Unsubscribe
    }


    public void addSpawn(RespawnController spawn)
    {
        _respawnList.Add(spawn);
       
        if (_spawnAllEnemies) spawn.spawnAllEnemies(_level);
    }

    /// <summary>
    /// Spawn all the enemies of this level.
    /// </summar
    private void spawnAllEnemies()
    {

        foreach (RespawnController respawn in _respawnList)
        {
            respawn.spawnAllEnemies(_level);
        }
    }

    /// <summary>
    /// Respawn the player on the last checkpoint.
    /// </summar
    private void respawnPlayerLastCheckpoint()
    {
        _spawnPoint.spawnPlayer();
        StartCoroutine(respawnEffect());

    }

    IEnumerator respawnEffect()
    {
        GameObject go = Instantiate(Resources.Load<GameObject>("Effects/RespawnEffect"), _playerController.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(4f);
        go.GetComponentsInChildren<ParticleSystem>()[0].Stop();
        Destroy(go,1f);
    }

    /// <summary>
    /// Sets new player's respawn point.
    /// </summar
    public void setRespawnPoint(RespawnController checkpoint)
    {
        if (_spawnPoint != null && _disableOldSpawns)//Mirem que no sigui el primer spawn.
        {
            _spawnPoint.destroyAllEnemies();
            _respawnList.Remove(_spawnPoint);
        }
        _spawnPoint = checkpoint;
       
    }

    /// <summary>
    /// Called when player dies. 
    /// </summary>
    public void tryAgain()
    {
        if (_easyMode || SoulsSystem.canRevive())
        {
            SoulsSystem.revive();
            UIEventHandler.soulCollected(SoulsSystem._soulCounter);
            //spawnAllEnemies();
            respawnPlayerLastCheckpoint();
            spawnAllEnemies();
        }
        else
        {
            UIEventHandler.disableBasicGUI();
            _deadMenu.pause();
            UIEventHandler.die();
        }
    }

    public static bool gamePaused()
    {
        return PauseMenu._gamePaused || DeadMenu._gamePaused || TabMenu._gameTabed;
    }

}
