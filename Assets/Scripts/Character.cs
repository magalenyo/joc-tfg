﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(AudioSource))]         //When the script is added to a new gameobject adds this component
//[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AnimatorController))]
public abstract class Character : MonoBehaviour
{
    protected CharacterStats _stats;

    //Actions
    protected Dictionary<string, bool> _actions;
    protected Dictionary<string, int> _animLayer;
    protected Dictionary<string, float> _animFloats;

    #region Character states
    protected string _attacking = "isAttacking";
    protected string _moving = "isMoving";
    protected string _jumping = "isJumping";
    protected string _crouch = "isCrouch";
    protected string _rolling = "isRolling";
    protected string _dead = "isDead";
    protected string _isRespawning = "isRespawning";
    protected string _isDying = "isDying";
    #endregion

    #region Character values
    protected string _speed = "Speed";
    #endregion

    #region Components
    protected Rigidbody _rb;
    protected AnimatorController _animController;
    #endregion

    #region Audio
    [Header("Character")]
    public int _nFootstepClips = Constants.AC_footseps;
    public AudioSource _audioSource;
    private AudioClip[] _footstepClips;
    
    #endregion


    void Awake()
    {
        _actions = new Dictionary<string, bool>();
        _animLayer = new Dictionary<string, int>();
        _animFloats = new Dictionary<string, float>();

      
    }

    protected virtual void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _animController = GetComponent<AnimatorController>();

        _actions = new Dictionary<string, bool>();
        _animLayer = new Dictionary<string, int>();
        _animFloats = new Dictionary<string, float>();

        _actions[_attacking] = false;
        _actions[_dead] = false;
        _actions[_isDying] = false;
        _actions[_moving] = false;
        setExtraActions();

        //_nFootstepClips = ;
        _footstepClips = new AudioClip[_nFootstepClips];
        _audioSource = GetComponent<AudioSource>();

        setFootstepSounds();
    }

    public void resetStats()
    {
        
        _actions[_dead] = false;
        _actions[_isDying] = false;
        setMaxHealth();
        if(this is PlayerController) UIEventHandler.healthChanged(getStat(Constants.currentHealth), getStat(Constants.maxHealth));
    }

    public virtual void takeDamage(float amount)
    {
        if (!_actions[_dead]) {
            float def = _stats.getStat(Constants.resPhys);
            if ((amount - def) > 0)
                _stats.decreaseStat(Constants.currentHealth, amount - def);

            if (_stats.getStat(Constants.currentHealth) <= 0)
            {
                _actions[_isDying] = true;
                _actions[_moving] = false;
                _animLayer["Die"] = 1;
            }
        }
    }

    protected abstract void die();
 

    public void setMaxHealth()
    {
        if (_stats != null)
            _stats.setStat(Constants.currentHealth, _stats.getStat(Constants.maxHealth));
    }

    public void increaseHealth(float plusHealth)
    {
        if (_stats != null)
        {
            _stats.increaseStat(Constants.currentHealth, plusHealth);
            float maxHealth = _stats.getStat(Constants.maxHealth);
            if (_stats.getStat(Constants.currentHealth) > maxHealth) _stats.setStat(Constants.currentHealth, _stats.getStat(Constants.maxHealth));
            UIEventHandler.healthChanged(_stats.getStat(Constants.currentHealth), _stats.getStat(Constants.maxHealth));
        }
    }

    public float getStat(string stat)
    {
        return _stats.getStat(stat);
    }

    protected abstract void initializeStats();

    protected abstract void setExtraActions();

    protected void updateAnimations()
    {
        _animController.updateAnimations(_actions, _animLayer, _animFloats);
    }

    #region Character events

    public void event_endedDie()
    {
        die();
    }

    #region Sound events
    public void event_leftFootstep()
    {
        playFootstepSound();
    }

    public void event_rightFootstep()
    {
        playFootstepSound();
    }

    public void event_leftFootstepCrouched()
    {
        playFootstepSound();
    }

    public void event_rightFootstepCrouched()
    {
        playFootstepSound();
    }
    #endregion
    #endregion

    #region Audio Controller

    /// <summary>
    /// Loads default Humanoid footsteps from resources sounds/footstep/footx
    /// </summary>
    private void setFootstepSounds()
    {
        for (int i = 1; i <= _nFootstepClips; i++)
        {
            _footstepClips[i-1] = Resources.Load<AudioClip>("sounds/footstep/foot" + i);
        }
    }

    /// <summary>
    /// Loads Path footsteps from already set resources sounds/path
    /// </summary>
    /// <param name="path"></param>
    protected void setFootstepSounds(string path)
    {
        _footstepClips = new AudioClip[_nFootstepClips];
        for (int i = 1; i <= _nFootstepClips; i++)
        {
            _footstepClips[i - 1] = Resources.Load<AudioClip>("sounds/"+ path + + i);
        }
    }

    private AudioClip getRandomFootstepClip()
    {
        return _footstepClips[UnityEngine.Random.Range(0, _footstepClips.Length)];
    }

    private void playFootstepSound()
    {
        try
        {
            AudioClip clip = getRandomFootstepClip();
            _audioSource.PlayOneShot(clip);
        }
        catch(Exception e)
        {
            Debug.Log("ERROR[Footstep Audio Cip]:" + e, this);
        }
    }

    protected AudioClip getRandomHitClip(AudioClip[] audioClips)
    {
        return audioClips[UnityEngine.Random.Range(0, audioClips.Length)];
    }


    protected void playAudio(AudioClip[] audioClips)
    {
        try
        {
            AudioClip clip = getRandomHitClip(audioClips);
            _audioSource.PlayOneShot(clip);
        }
        catch (Exception e)
        {
            Debug.Log("ERROR[Hit Audio Cip]:" + e, this);
        }
    }

    #endregion

    public abstract void teleportTo(Vector3 _position);

    public abstract void respawn(Vector3 _position);

    public CharacterStats getStats()
    {
        return _stats;
    }

    public bool isDead()
    {
        return _actions[_dead];
    }

    public bool isDying()
    {
        return _actions[_isDying];
    }


}
