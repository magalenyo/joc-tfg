﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(ProjectileController))]
public class SpikesThrower_Trap : Trap
{
    private ProjectileController _projectileController;

    public string _ProjectileTag = "KunaiTrap";
    public float _increaseDistance = 1f;
    public float _superiorProjectiles = 1;
    public float _inferiorProjectiles = 1;


    private void Start()
    {
        _projectileController = GetComponent<ProjectileController>();
    }

    public override void activateTrap()
    {
        Vector3 shotPoint = transform.position;
        instanceProjectile(shotPoint);

        //Superior projectiles
        for (int i = 0;i< _superiorProjectiles; i++)
        {
            shotPoint = new Vector3(shotPoint.x, shotPoint.y+ _increaseDistance, shotPoint.z);
            instanceProjectile(shotPoint);
        }

        //Inferior Projectiles
        shotPoint = transform.position;
        for (int i = 0; i < _superiorProjectiles; i++)
        {
            shotPoint = new Vector3(shotPoint.x, shotPoint.y - _increaseDistance, shotPoint.z);
            instanceProjectile(shotPoint);
        }
    }

    private void instanceProjectile(Vector3 position){
        GameObject go = _projectileController.shoot(Resources.Load<GameObject>(_ProjectileTag), position);
        
    }

}
