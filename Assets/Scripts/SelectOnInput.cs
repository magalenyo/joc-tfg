﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Class used to selet on input depending whether if is being played with a controller or mouse and keyboard
/// </summary>
public class SelectOnInput : MonoBehaviour
{
    public EventSystem _eventSystem;            //detector
    public GameObject _selectedObject;          //selected object (set to the first)

    private bool _buttonSelected;

    /// <summary>
    /// Initializes values
    /// </summary>
    void Start()
    {
        _buttonSelected = false;
    }

    /// <summary>
    /// Sets the next selected gameObject
    /// </summary>
    void Update()
    {
        if(Input.GetAxisRaw("Vertical") != 0 && _buttonSelected == false)
        {
            _eventSystem.SetSelectedGameObject(_selectedObject);
            _buttonSelected = true;
        }
    }

    /// <summary>
    /// Deselects
    /// </summary>
    private void OnDisable()
    {
        _buttonSelected = false;
    }
}
