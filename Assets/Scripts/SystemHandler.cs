﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemHandler : MonoBehaviour
{
    public delegate void PlayerAddExperienceEventHandler(float xp);
    public static event PlayerAddExperienceEventHandler onExperienceGained;

    public delegate void PlayerLevelUpEventHandler();
    public static event PlayerLevelUpEventHandler tryLevelUp;

    public static void addExperience(float xp)
    {
        onExperienceGained(xp);
    }

    public static void levelUp()
    {
        tryLevelUp();
    }


}
