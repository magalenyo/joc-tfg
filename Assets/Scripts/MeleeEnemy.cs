using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using System;

/// <summary>
/// Melee Enemy Controller
/// </summary>
public class MeleeEnemy : Enemy
{
    public Transform _target;
    
    private MeleeWeaponController _weapon;

    public float _distanceToTarget;
    public float _lookRadius ;

    //IA
    public NavMeshAgent _agent;

    //public bool potAtacar;
    public GameObject _weaponObject;

    #region Sound
    public AudioClip _chainClip;
    #endregion


    /// <summary>
    /// Initializes stats
    /// </summary>
    private void Awake()
    {
        initializeStats();

    }

    /// <summary>
    /// Initializes values and components
    /// </summary>
    protected override void Start()
    {
        base.Start();

        _target = PlayerController.instance.getPosition();
        _agent = GetComponent<NavMeshAgent>();
        _lookRadius = 7f;
        _weapon = _weaponObject.GetComponent<MeleeWeaponController>();
    }

    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        if (!isDying() && !isDead() && !isPlayerDead())
        {
            if (_agent.isStopped) _agent.isStopped = false;
            //potAtacar = _distanceToTarget > _agent.stoppingDistance;
            _distanceToTarget = Vector3.Distance(_target.position, transform.position);

            attack();

            if (!_actions[_attacking] && !isDead() && !isPlayerDead())
            {
                move();
                if (_distanceToTarget <= _lookRadius && _distanceToTarget > _agent.stoppingDistance) faceTarget();
                //if(transform.forward == Vector3.left || transform.forward == Vector3.right)
                //{
                //    faceTarget();
                //}
            }
            //print(Vector3.Dot(transform.position, _target.transform.position));
        
            //if (_canFaceTarget) faceTarget();
            //faceTarget();
            //print(transform.forward);
        }
        else
        {
            _agent.isStopped = true;
            _actions[_moving] = false;
        }
            

        updateAnimations();
    }

    private void FixedUpdate()
    {
        if (transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        }
    }


    //private bool canFaceTarget()
    //{
    //    Vector3 dirFromAtoB = (_target.position - transform.position).normalized;
    //    float dotProd = Vector3.Dot(dirFromAtoB, transform.forward);

    //    if (dotProd > 0.9)
    //    {
    //        return true;
    //    }
    //    else return false;
    //}

    private void move()
    {
        
        if (_distanceToTarget <= _lookRadius && _distanceToTarget > _agent.stoppingDistance)
        {
            _agent.SetDestination(_target.position);
            _actions[_moving] = true;
            _agent.isStopped = false;
        }
        else
        {
            _agent.isStopped = true;
            _actions[_moving] = false;
        }
        
        _actions[_attacking] = false;
        _animFloats[_speed] = Math.Abs(_agent.speed);
    }


    private void attack()
    {
        if (!_actions[_attacking] && _distanceToTarget <= _agent.stoppingDistance && alignedWithTarget())
        {
            _audioSource.PlayOneShot(_chainClip);
            //_weapon.borrar();
            _actions[_attacking] = true;
        }
    }

    private bool alignedWithTarget()
    {
        Vector3 aux = new Vector3(Mathf.Round(transform.forward.x), Mathf.Round(transform.forward.y), Mathf.Round(transform.forward.z));
        return aux == Vector3.left || aux == Vector3.right;
    }

    void faceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    #region Debug
    //Debug, dibuixem l'area d'awake
    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawWireSphere(transform.position, _lookRadius);
    //}
    #endregion

    #region Events
    public void event_endAttacking()
    {
        _weapon.stopAttack();
        //_actions[_attacking] = false;
    }

    public void event_swoosh()
    {
        _weapon.attack();
    }

    public void event_endAttackAnimation()
    {
        _actions[_attacking] = false;
    }
    #endregion

    protected override void initializeStats()
    {
        Dictionary<string,float> stats = new Dictionary<string, float>();
        stats.Add(Constants.maxHealth, Constants.Stats_Fighter_maxHealth);
        stats.Add(Constants.currentHealth, Constants.Stats_Fighter_maxHealth);
        stats.Add(Constants.resMagic, Constants.Stats_Fighter_resMagic);
        stats.Add(Constants.resPhys, Constants.Stats_Fighter_resPhys);
        stats.Add(Constants.moveSpeed, Constants.Stats_Fighter_moveSpeed);
        stats.Add(Constants.cooldown, Constants.Stats_Fighter_cooldown);
        stats.Add(Constants.strength, Constants.Stats_Fighter_strength);
        stats.Add(Constants.magic, Constants.Stats_Fighter_magic);

        stats.Add(Constants.experience, Constants.Stats_Fighter_experience);
        stats.Add(Constants.soulCounter, Constants.Stats_Fighter_souls);
        _stats = new CharacterStats(stats);
    }

    protected override void setExtraActions()
    {
        
    }

    public override void teleportTo(Vector3 _position)
    {
        _agent.Warp(_position);
    }
}