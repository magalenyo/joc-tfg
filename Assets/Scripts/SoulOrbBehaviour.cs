﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulOrbBehaviour : MonoBehaviour
{
    private Transform _destination;
    private float _smooth = 3f;
    private MaskFollowerBehaviour _mascarea = MaskFollowerBehaviour.instance;

    void Start()
    {
        if (_mascarea.gameObject.transform == null) _destination = PlayerController.instance.transform;
        else _destination = _mascarea.gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, _destination.position, _smooth * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == Constants.Tag_Mascarea)
        {
            Destroy(gameObject);
        }
    }
}
