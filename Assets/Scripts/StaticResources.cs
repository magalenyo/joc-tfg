﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class that loads Resources folder resources such as Enemy Prefabs, Player, etc.
/// </summary>
public static class StaticResources
{
    #region Object names
    public const string _enemyRanged1 = "enemy_Ranged1";
    public const string _enemyMelee1 = "enemy_melee1";
    public const string _enemySeeker1 = "enemy_seeker1";
    public const string _enemyFlyingRanged1 = "enemy_flyingRanged1";
    #endregion

    #region Objects
    private static Object _oEnemyRanged1;
    private static Object _oEnemyMelee1;
    private static Object _oEnemySeeker1;
    private static Object _oEnemyFlyingRanged1;
    #endregion

    public static bool firstTime = true;

    public static void setfalse()
    {
        firstTime = false;
    }

    /// <summary>
    /// Returns the object(prefab) of the enemyId resources and also saves it, to not load it again when requested.
    /// </summary>
    public static Object getEnemyPrefab(string enemyId)
    {
        Object enemy = null;

        switch (enemyId) {
            case _enemyRanged1:

                if (_oEnemyRanged1 == null)
                    _oEnemyRanged1 = Resources.Load("Enemies/" + enemyId);
                enemy = _oEnemyRanged1;
                break;

            case _enemyMelee1:

                if (_oEnemyMelee1 == null)
                    _oEnemyMelee1 = Resources.Load("Enemies/" + enemyId);
                enemy = _oEnemyMelee1;
                break;

            case _enemySeeker1:

                if (_oEnemySeeker1 == null)
                    _oEnemySeeker1 = Resources.Load("Enemies/" + enemyId);
                enemy = _oEnemySeeker1;
                break;

            case _enemyFlyingRanged1:

                if (_oEnemyFlyingRanged1 == null)
                    _oEnemyFlyingRanged1 = Resources.Load("Enemies/" + enemyId);
                enemy = _oEnemyFlyingRanged1;
                break;

        }
        return enemy;

    }

    public static Vector3 getLevelPosition(int level)
    {
        if (level == 2)
            return (new Vector3(40, 110, 0));
        else return (new Vector3(50, 120, 0));
    }
       
}
