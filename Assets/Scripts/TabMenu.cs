﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller of the Tab Menu. This loads and unloads tab menu content: the skill tree.
/// </summary>
public class TabMenu : MonoBehaviour
{
    public static bool _gameTabed;

    [Header("PauseMenu")]
    [SerializeField] private PauseMenu _pauseMenu;

    [Header("Skill Tree Script")]
    [SerializeField] private SkillTree _skillTree;

    [Header("UIs")]
    [SerializeField] private GameObject _tabUI;

    void Start()
    {
        _gameTabed = false;
    }

    /// <summary>
    /// Check if the tab key has been pressed and if the game is not at the Pause Menu, tabs the game, or untabs it
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && !_pauseMenu.isPaused())
        {
            if (_gameTabed)
            {
                resume();
            }
            else if (!_gameTabed)
            {
                pause();
            }
        }
    }

    /// <summary>
    /// UnTabs the game, which means it desables the Tab Menu Views, enables the Basic GUI and also unfreezes the game (timeScale = 1). Also, makes the skill tree points empty
    /// </summary>
    public void resume()
    {
        _skillTree.emptySkillPoints();
        _tabUI.SetActive(false);
        //disable();
        UIEventHandler.enableBasicGUI();

        Time.timeScale = 1; //pause game
        _gameTabed = false;
    }

    /// <summary>
    /// Tabs the game, which means it enables the Tab Menu Views, disables the Basic GUI and also freezes the game (timeScale = 0)
    /// </summary>
    public void pause()
    {
        UIEventHandler.disableBasicGUI();
        //enable();
        _tabUI.SetActive(true);


        Time.timeScale = 0; //pause game
        _gameTabed = true;
    }

    /// <summary>
    /// Disables the Tab Menu Views and enables Basic GUI
    /// </summary>
    private void disable()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++) gameObject.transform.GetChild(i).gameObject.SetActive(false);
    }

    /// <summary>
    /// Disables basic GUI and enables the Tab Menu Views
    /// </summary>
    private void enable()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++) gameObject.transform.GetChild(i).gameObject.SetActive(true);
    }

    /// <summary>
    /// Returns true if isTabed
    /// </summary>
    public bool isTabed()
    {
        return _gameTabed;
    }
}
