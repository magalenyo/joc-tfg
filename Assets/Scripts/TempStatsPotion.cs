﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Consumible abstract class that increases and decreases a specific stat of the consumer
/// </summary>
public abstract class TempStatsPotion : Consumible
{
    [SerializeField]
    protected int _ExtraStat;
    [SerializeField]
    protected int _EffectsTime;
    protected string _Stat;
    [SerializeField]
    private string _effect;
    private Consumer _consumer;
    private GameObject _effectgo;

    /// <summary>
    /// Increases consumer's stat and makes it disappear in time
    /// </summary>
    public override void consume(Consumer consumer)
    {
        _consumer = consumer;
        _consumer.getStats().increaseStat(_Stat, _ExtraStat);
        _consumer.drink();

        //Add particle effects (instantiate)
        if (!string.IsNullOrEmpty(_effect))
        {
            if (_effect != null) Destroy(_effectgo);

            _effectgo = Instantiate(Resources.Load<GameObject>("Effects/" + _effect), _consumer.transform);
            _effectgo.transform.parent = _consumer.transform;
            //_consumer.gameObject.(go);
            Destroy(_effectgo, _EffectsTime);
        }
        Invoke("decreaseStat", _EffectsTime);


    }

    /// <summary>
    /// Decreases consumer's increased stat
    /// </summary>
    private void decreaseStat() { 

        _consumer.getStats().decreaseStat(_Stat, _ExtraStat);
        print(_consumer.getStats());
       
        //Reemove particle effects

    }
}
