﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampIntensityController : MonoBehaviour
{
    [SerializeField] private Light _light;
    [SerializeField] [Range(0, 2)] private float _minIntensity = 1f;
    [SerializeField] [Range(0, 2)] private float _maxIntensity = 1.5f;
    [SerializeField] private float _duration = 1f;
    [SerializeField] private float _waitTime = 1f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(fadeInAndOutRepeat());
    }

    IEnumerator fadeInAndOutRepeat()
    {
        while (true)
        {
            //Fade out
            yield return fadeInAndOut(false);

            //Wait
            yield return new WaitForSeconds(_waitTime);

            //Fade-in 
            yield return fadeInAndOut(true);
        }
    }

    IEnumerator fadeInAndOut(bool fadeIn)
    {
        float counter = 0f;

        //Set Values depending on if fadeIn or fadeOut
        float a, b;

        if (fadeIn)
        {
            a = _minIntensity;
            b = _maxIntensity;
        }
        else
        {
            a = _maxIntensity;
            b = _minIntensity;
        }

        float currentIntensity = _light.intensity;

        while (counter < _duration)
        {
            counter += Time.deltaTime;

            _light.intensity = Mathf.Lerp(a, b, counter / _duration);

            yield return null;
        }
    }
}
