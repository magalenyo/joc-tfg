﻿using UnityEngine;

public class MyEventHandler : MonoBehaviour
{
    private PlayerController _playerController;

    // Start is called before the first frame update
    void Start()
    {
        _playerController = GetComponent<PlayerController>();
    }

    public void event_throwSpell()
    {
        _playerController.useSpell();
       
    }


}
