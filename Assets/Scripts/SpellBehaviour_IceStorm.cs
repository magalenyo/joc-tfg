﻿using UnityEngine;

/// <summary>
/// SpellBehaviour of IceStorm implements the behaviour of each shard shot 
/// </summary>
public class SpellBehaviour_IceStorm : Projectile
{
    /// <summary>
    /// Initializes values
    /// </summary>
    public SpellBehaviour_IceStorm()
    {
        _lifeTimeLong = Constants.Lt_Long_IceStorm;
        _lifeTimeShort = Constants.Lt_Short_IceStorm;
        _projectileDmg = Constants.Dmg_IceStorm;
        _shootForce = Constants.Speed_IceStorm;
    }

    /// <summary>
    /// Sets shard directions and makes sets destroy timer, also ignores collission of layer 11 (it's own)
    /// </summary>
    protected override void Start()
    {
        base.Start();

        _rb.velocity = Vector3.down * _shootForce;
        Physics.IgnoreLayerCollision(Constants.Layer_IceStorm, Constants.Layer_IceStorm);
    }

    /// <summary>
    /// Stays in direction, destroys the object
    /// </summary>
    void Update()
    {
        if (!_hitSomething)
        {
            //transform.rotation = Quaternion.LookRotation(_rb.velocity * -1);
        }

        if (_hitEnemy && _stickCollider == null) Destroy(gameObject);
    }

}
