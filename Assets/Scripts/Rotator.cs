﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rotspeed;
    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(rotSpeed, 0, 0) ;
        transform.Rotate(new Vector3(0, rotspeed, 0) * Time.deltaTime);
    }
}
