﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockTrap : Trap
{
    public override void activateTrap()
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = false;
    }
    
}
