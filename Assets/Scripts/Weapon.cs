﻿/// <summary>
/// Weapon interface
/// </summary>
public interface Weapon
{
    /// <summary>
    /// Returns the attack value of the weapon
    /// </summary>
    float getAttackValue();
}

