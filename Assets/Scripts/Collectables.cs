﻿using System;

public class Collectables
{
    private Collectables(string value) { Value = value; }

    public string Value { get; set; }

    public static Collectables Chest { get { return new Collectables("Chest"); } }
    public static Collectables Heart { get { return new Collectables("Heart"); } }
}
