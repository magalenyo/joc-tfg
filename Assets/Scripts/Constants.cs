﻿using System.Collections.Generic;

public class Constants
{
    #region Tags
    public static string Tag_Player = "Player";
    public static string Tag_Kunai = "Kunai";
    public static string Tag_Enemy = "Enemy";
    public static string Tag_Chest = "Chest";
    public static string Tag_ChestInteract = "ChestInteract";
    public static string Tag_Mascarea = "Mascarea";
    #endregion

    #region BasicStats
    public static string maxHealth = "maxHealth";
    public static string currentHealth = "currentHealth";
    public static string resMagic = "resMagic";
    public static string resPhys = "resPhys";
    public static string moveSpeed = "moveSpeed";
    public static string cooldown = "cooldown";
    public static string strength = "strength";
    public static string magic = "magic";

    public static string experience = "experience";
    public static string soulCounter = "soulCounter";
    #endregion

    #region Spells
    public static string Spell_fireball = "Spell_fireball";
    public static string Spell_IceStorm = "Spell_IceStorm";
    #endregion

    #region Damages
    public const float Dmg_Fireball = 15f;
    public const float Dmg_Kunai = 10f;
    //public const float Dmg_Sword = 10f;
    public const float Dmg_ThrowableAxe = 10f;
    public const float Dmg_IceStorm = 4f;
    public const float Dmg_ProjBomb = 15f;
    #endregion

    #region Weapons Speed
    public const float Speed_Fireball = 15f;
    public const float Speed_Kunai = 14f;
    public const float Speed_Sword = 10f;
    public const float Speed_ThrowableAxe = 20f;
    public const float Speed_IceStorm = 12f;
    public const float Speed_ProjBomb = 4f;
    #endregion

    #region Projectiles Lifetime
    public const float Lt_Short_FireBall = 0f;
    public const float Lt_Long_Fireball = 10f;
    public const float Lt_Short_Kunai = 3f;
    public const float Lt_Long_Kunai = 5f;
    public const float Lt_Short_ThrowableAxe = 2f;
    public const float Lt_Long_ThrowableAxe = 6f;
    public const float Lt_Short_IceStorm = 3f;
    public const float Lt_Long_IceStorm = 5f;
    public const float Lt_Long_ProjBomb = 5f;
    public const float Lt_Short_ProjBomb = 5f;
    #endregion

    #region Spell Costs in Skill Points
    public const int SPCost_Fireball = 3;
    public const int SPCost_IceStorm = 4;
    #endregion

    #region Player Base Stats
    public static float Stats_Player_maxHealth = 100f;
    public static float Stats_Player_resMagic = 5f;
    public static float Stats_Player_resPhys = 0f;
    public static float Stats_Player_moveSpeed = 1f;
    public static float Stats_Player_cooldown = 0f;
    public static float Stats_Player_strength = 5f;
    public static float Stats_Player_magic = 1f;
    #endregion

    #region Player Increase Stat factor
    public static float Factor_Player_maxHealth = 20;
    public static float Factor_Player_resMagic = 1;
    public static float Factor_Player_resPhys = 1;
    public static float Factor_Player_moveSpeed = 0.1f;
    public static float Factor_Player_cooldown = 2;
    public static float Factor_Player_strength = 1f;
    public static float Factor_Player_magic = 4;
    #endregion

    #region Player Increase Spell Factor
    public static float Factor_Spell_Fireball = 4f;
    public static float Factor_Spell_IceStorm = 2f;
    #endregion

    #region Melee Enemy Base Stats
    public static float Stats_Fighter_maxHealth = 45f;
    public static float Stats_Fighter_resMagic = 2f;
    public static float Stats_Fighter_resPhys = 1f;
    public static float Stats_Fighter_moveSpeed = 2f;
    public static float Stats_Fighter_cooldown = 0f;
    public static float Stats_Fighter_strength = 5f;
    public static float Stats_Fighter_magic = 1f;
    public static int Stats_Fighter_experience = 5;
    public static int Stats_Fighter_souls = 1;
    #endregion

    #region bossMaskarea Base Stats
    public static float Stats_BossMaskarea_maxHealth = 500f;
    public static float Stats_BossMaskarea_resMagic = 2f;
    public static float Stats_BossMaskarea_resPhys = 5f;
    public static float Stats_BossMaskarea_moveSpeed = 2f;
    public static float Stats_BossMaskarea_cooldown = 0f;
    public static float Stats_BossMaskarea_strength = 10f;
    public static float Stats_BossMaskarea_magic = 1f;
    public static int Stats_BossMaskarea_experience = 1;
    public static int Stats_BossMaskarea_souls = 1;
    #endregion

    #region Enemy Axe Thrower Base Stats
    public static float Stats_AxeThrower_maxHealth = 35f;
    public static float Stats_AxeThrower_resMagic = 2f;
    public static float Stats_AxeThrower_resPhys = 2f;
    public static float Stats_AxeThrower_moveSpeed = 2f;
    public static float Stats_AxeThrower_cooldown = 0f;
    public static float Stats_AxeThrower_strength = 10f;
    public static float Stats_AxeThrower_magic = 1f;
    public static int Stats_AxeThrower_experience = 3;
    public static int Stats_AxeThrower_souls = 1;
    #endregion

    #region Seeker Base Stats
    public static float Stats_Seeker_maxHelth = 10f;
    public static float Stats_Seeker_resMagic = 0f;
    public static float Stats_Seeker_resPhys = 0f;
    public static float Stats_Seeker_moveSpeed = 3.5f;
    public static float Stats_Seeker_strength = 10f;
    public static int Stats_Seeker_experience = 2;
    public static int Stats_Seeker_souls = 1;
    #endregion

    #region FlyingRanged Base Stats
    public static float Stats_FlyingRanged_maxHelth = 10f;
    public static float Stats_FlyingRanged_resMagic = 0f;
    public static float Stats_FlyingRanged_resPhys = 0f;
    public static float Stats_FlyingRanged_moveSpeed = 2f;
    public static float Stats_FlyingRanged_strength = 1f;
    public static int Stats_FlyingRanged_experience = 2;
    public static int Stats_FlyingRanged_souls = 1;
    #endregion

    #region Audio clips counter
    public const int AC_footseps = 11;
    public const int AC_projectileHits = 1;
    #endregion

    #region Layers
    public static int Layer_Player = 9;
    public static int Layer_Enemy = 10;
    public static int Layer_SlimeWeapon = 11;
    public static int Layer_SlimePrefab = 12;
    public static int Layer_Mascarea = 13;
    public static int Layer_IceStorm = 14;
    #endregion
}
