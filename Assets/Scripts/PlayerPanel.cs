﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

/// <summary>
/// GUI Main controller. Saves all the references to the GUI and the methods to change the values displayed.
/// </summary>
public class PlayerPanel : MonoBehaviour
{
    [Header("HP")]
    [SerializeField] private TextMeshProUGUI _health;
    [SerializeField] private Image _healthFill;

    [Header("Experience")]
    [SerializeField] private TextMeshProUGUI _experience;
    [SerializeField] private TextMeshProUGUI _level;
    [SerializeField] private Image _experienceFill;

    [Header("Level message")]
    [SerializeField] private GameObject _levelUpMessage;

    [Header("Skill Points")]
    [SerializeField] private TextMeshProUGUI _skillPoints;

    [Header("Souls")]
    [SerializeField] private TextMeshProUGUI _souls;

    [Header("Equipment")]
    [SerializeField] private Image _equipmentImage;
    [SerializeField] private TextMeshProUGUI _equipmentCounterText;

    [Header("UI")]
    [SerializeField] private GameObject _soulUI;
    [SerializeField] private GameObject _healthUI;
    [SerializeField] private GameObject _equipmentUI;
    [SerializeField] public GameObject _deadMenuUI;

    public static PlayerPanel instance;
    //[Header("BOSS UI")]
    //[SerializedField] private GameObject _bossUI;


    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(transform.root.gameObject);
    }
    /// <summary>
    /// Handler
    /// </summary>
    void Start()
    {

        UIEventHandler.onPlayerHealthChanged += updateHealth;   //set the listener of the delegate, this is a subscribed element
        UIEventHandler.onExperienceGained += updateExperience;
        UIEventHandler.onSoulCollected += collectSoul;
        UIEventHandler.onLevelUp += levelUpMessage;
        UIEventHandler.onSkillPointUsed += useSkillPoint;
        UIEventHandler.onEquipmentChanged += changeEquipment;
        UIEventHandler.onBasicGUIDisabled += disableBasicGUI;
        UIEventHandler.onBasicGUIEnabled += enableBasicGUI;
        UIEventHandler.onDead += enableDeadMenu;

        //>
        PlayerController.OnEquipmentChange += changeEquipment;
    }

    private void OnDestroy()
    {
        UIEventHandler.onPlayerHealthChanged -= updateHealth;   //set the listener of the delegate, this is a subscribed element
        UIEventHandler.onExperienceGained -= updateExperience;
        UIEventHandler.onSoulCollected -= collectSoul;
        UIEventHandler.onLevelUp -= levelUpMessage;
        UIEventHandler.onSkillPointUsed -= useSkillPoint;
        UIEventHandler.onEquipmentChanged -= changeEquipment;
        UIEventHandler.onBasicGUIDisabled -= disableBasicGUI;
        UIEventHandler.onBasicGUIEnabled -= enableBasicGUI;
        UIEventHandler.onDead -= enableDeadMenu;

        //>
        PlayerController.OnEquipmentChange -= changeEquipment;
    }

    /// <summary>
    /// Displays a text of the currentHealth and displays it in relation with maxHealth (retocar definicioxd)
    /// </summary>
    void updateHealth(float currentHealth, float maxHealth)
    {
        if(currentHealth >= 0)
        {
            this._health.text = Math.Round(currentHealth).ToString();
            if (currentHealth == 0 || maxHealth == 0) this._healthFill.fillAmount = 0;
            else this._healthFill.fillAmount = currentHealth / maxHealth;
        }
        else
        {
            this._health.text = "0";
            this._healthFill.fillAmount = 0;
        }
    }

    /// <summary>
    /// Displays an ammount of experience, the maxExperience until next level and the current level.
    /// </summary>
    void updateExperience(float experience, float maxExperience, int level)
    {
        this._experience.text = experience + " xp";
        this._level.text = level.ToString();
        if (experience == 0 || maxExperience == 0) this._experienceFill.fillAmount = 0;
        else this._experienceFill.fillAmount = experience / maxExperience;
    }

    /// <summary>
    /// Displays the number of souls.
    /// </summary>
    void collectSoul(int souls)
    {
        if (souls >= 0) _souls.text = souls.ToString();
        else _souls.text = "0";
    }

    /// <summary>
    /// Updates the number of current skill points. And starts a coroutine.
    /// </summary>
    void levelUpMessage()
    {
        useSkillPoint();
        StartCoroutine(levelUp());
    }

    /// <summary>
    /// Coroutine that makes the level up message visible and makes it not visible after 2 seconds.
    /// </summary>
    IEnumerator levelUp()
    {
        _levelUpMessage.SetActive(true);
        yield return new WaitForSeconds(2);
        _levelUpMessage.SetActive(false);
    }

    /// <summary>
    /// Displays the current ammount of skill points available.
    /// </summary>
    void useSkillPoint()
    {
        _skillPoints.text = ExperienceSystem._skillPoints + " skill points";
    }

    /// <summary>
    /// Changes the current equipment image to the rscImage and refreshes the ammount of equpiment.
    /// </summary>
    void changeEquipment(string rscImage, int counter)
    {
        _equipmentImage.sprite = Resources.Load<Sprite>("UI/Equipment/" + rscImage);
        _equipmentCounterText.text = counter.ToString();
    }

    /// <summary>
    /// Sets the soulUI, healthUI, equipmentUI and levelUpMessage active to false
    /// </summary>
    void disableBasicGUI()
    {
        _soulUI.SetActive(false);
        _healthUI.SetActive(false);
        _equipmentUI.SetActive(false);
        _levelUpMessage.SetActive(false);
    }

    /// <summary>
    /// Sets the soulUI, healthUI and equipmentUI active to true
    /// </summary>
    void enableBasicGUI()
    {
        _soulUI.SetActive(true);
        _healthUI.SetActive(true);
        _equipmentUI.SetActive(true);
    }

    /// <summary>
    /// Set the DeadMenu active.
    /// </summary>
    void enableDeadMenu()
    {
        _deadMenuUI.SetActive(true);

    }

}
