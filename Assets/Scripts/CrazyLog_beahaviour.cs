﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazyLog_beahaviour : MonoBehaviour
{

    private bool goDown,goUp;
    private Vector3 downPosition, upPosition;

    public float _DownSmooth = 0.3f;
    public float _UpSmooth = 4f;
    public float _downTargetValue = 3f;

    // Start is called before the first frame update
    void Start()
    {
        goDown = false;
        goUp = false;
        downPosition = new Vector3(transform.position.x, transform.position.y - _downTargetValue, transform.position.z);
        upPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    private void Update()
    {
        if (goDown)
            transform.position =  Vector3.Lerp(transform.position, downPosition, _DownSmooth * Time.deltaTime); //ha de tenir un smooth major (10f)
       
        if (goUp && (transform.position != upPosition))
            transform.position = Vector3.Lerp(transform.position, upPosition, _UpSmooth * Time.deltaTime); //ha de tenir un smooth major (10f)
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Constants.Tag_Player)
        {
            goDown = true;
            goUp = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Constants.Tag_Player)
        {
            goDown = false;
            goUp = true;
        }
    }
}
