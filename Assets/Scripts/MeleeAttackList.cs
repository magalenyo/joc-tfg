﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackList
{
    private List<string> _comboList;
    private List<MeleeWeaponController> _meleeWeaponList;

    public MeleeAttackList()
    {
        _comboList = new List<string>();
        _meleeWeaponList = new List<MeleeWeaponController>();
    }

    /// <summary>
    /// Returns the tupl of the attack and the weapon used to do that attack on index idx.
    /// </summary>
    public (string, MeleeWeaponController) get(int idx)
    {
        return (_comboList[idx], _meleeWeaponList[idx]);
    }

    /// <summary>
    /// Returns the attack string on the position idx.
    /// </summary>
    public string getCombo(int idx)
    {
        return _comboList[idx];
    }

    /// <summary>
    /// Returns the weapon used to do the attack on the position idx..
    /// </summary>
    public MeleeWeaponController getWeapon(int idx)
    {
        return _meleeWeaponList[idx];
    }

    /// <summary>
    /// Adds the attack _combo and the weapon used to make it _useadWeapon.
    /// </summary>
    public void add(string _combo, MeleeWeaponController _usedWeapon)
    {
        _comboList.Add(_combo);
        _meleeWeaponList.Add(_usedWeapon);
    }

    /// <summary>
    /// Returns the total of attacks.
    /// </summary>
    public int size()
    {
        return _comboList.Count;
    }
}