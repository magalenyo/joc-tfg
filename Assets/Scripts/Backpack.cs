﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class Backpack
{
    public List<Consumible> _consumibleList;
    public List<int> _consumibleCount;
    private Consumer _consumer;

    private int _equiped;

    public Backpack(Consumer consumer)
    {

        _equiped = -1;
        _consumibleList = new List<Consumible>();
        _consumibleCount = new List<int>();
        _consumer = consumer;

    }

    /// <summary>
    /// If euiped !=-1 consume the object equiped.
    /// </summary>
    public bool consumeEquiped()
    {
       if (_equiped >= 0)
            return consumeObj(_consumibleList[_equiped]);
        return false;
    }

    public (Consumible,int) current()
    {
        if (_equiped != -1)
            return (_consumibleList[_equiped],_consumibleCount[_equiped]);
        return (null,-1);
    }

    public void equipNext()
    {
        if (_consumibleList.Count >= 0)
        {
            _equiped++;
            if (_equiped >= _consumibleList.Count)
                _equiped = 0;
        }
    }

    public void equipPrevious()
    {
        if (_consumibleList.Count >= 0)
        {
            _equiped--;
            if (_equiped < 0)
                _equiped = _consumibleList.Count - 1;
        }
    }

    public void addNObject(Consumible _newObj, int num)
    {
        int index = containsType(_newObj);
        if (index != -1)
        {
            _consumibleCount[index] += num;
        }
        else
        {
            _consumibleList.Add(_newObj);
            _consumibleCount.Add(num);
        }
    }


    //Given a consumible returns true if the object was consumed else false;
    public bool consumeObj(Consumible consumible)
    {
      
        int idx = containsType(consumible);

        if (idx == -1)
            return false;
        else if (_consumibleCount[idx] > 0)
        {
            _consumibleCount[idx] -= 1;
            _consumibleList[idx].consume(_consumer);
            if (_consumibleCount[idx] == 0)
            {
                //_consumibleList.RemoveAt(idx);
                //_consumibleCount.RemoveAt(idx);
                //_equiped = -1;
            }
            return true;
        }

        return false;//No s ha consumit res.
        
    }

   
    /// <summary>
    /// If the list contains one object of the ral type of cons returns the index, else returns -1
    /// </summary>
    private int containsType(Consumible cons)
    {
        int i = 0;
        bool found = false;
        while (i < _consumibleList.Count && !found)
        {
            found = (_consumibleList[i].GetType() == cons.GetType());
            i++;
        }
        i = found ? i - 1 : -1;

        return i;
    }
}
