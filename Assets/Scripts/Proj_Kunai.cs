﻿using UnityEngine;

/// <summary>
/// Kunai Projectile Controller
/// </summary>
public class Proj_Kunai : Projectile
{
    /// <summary>
    /// Initializes values
    /// </summary>
    public Proj_Kunai()
    {  
        _lifeTimeLong = Constants.Lt_Long_Kunai;
        _lifeTimeShort = Constants.Lt_Short_Kunai;
        _projectileDmg = Constants.Dmg_Kunai;
        _shootForce = Constants.Speed_Kunai;
    }

    /// <summary>
    /// Sets kunai directions and makes sets destroy timer
    /// </summary>
    protected override void Start()
    {
        //_rb = GetComponent<Rigidbody>();
        base.Start();
        transform.rotation = Quaternion.LookRotation(_rb.velocity * -1);

        //_rb.velocity = _player.transform.forward * _shootForce;

        //destroyProjectile(_lifeTimeLong);
    }

    /// <summary>
    /// Stays in direction, destroys the object if it hits something
    /// </summary>
    void Update()
    {
        if (!_hitSomething) transform.rotation = Quaternion.LookRotation(_rb.velocity * -1);

        if (_hitEnemy && _stickCollider == null) Destroy(gameObject);
    }

    protected override void stick(Collision collision)
    {
        //ParticleSystem[] ps = gameObject.getco
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
        {
            if(ps.name != "Point") ps.gameObject.SetActive(false);
        }
        base.stick(collision);

    }

}
