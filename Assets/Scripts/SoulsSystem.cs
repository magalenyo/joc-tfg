﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Static class that stores the Soul System.
/// </summary>
public static class SoulsSystem
{
    public static int _soulCounter = 1;         //soul counter
    public static int _requiredSouls = 1;       //required souls to spawn

    /// <summary>
    /// Adds souls ammount to the souls counter
    /// </summary>
    public static void addSoul(int souls)
    {
        _soulCounter += souls;
    }

    /// <summary>
    /// Returns true if the soulcounter is greater or equal than the required souls
    /// </summary>
    public static bool canRevive()
    {
        return _soulCounter >= _requiredSouls;
    }

    /// <summary>
    /// Sets the next amount of required souls to spawn and resets the counter
    /// </summary>
    public static void revive()
    {
        setNextRequiredSouls();
        resetSouls();
    }

    /// <summary>
    /// SoulCounter - Required souls
    /// </summary>
    private static void resetSouls()
    {
        _soulCounter -= _requiredSouls;
    }

    /// <summary>
    /// Calculates the ammount of souls required to respawn
    /// </summary>
    private static void setNextRequiredSouls()
    {
        _requiredSouls = 1 + (int) Math.Truncate(_requiredSouls * 1.2f);
    }

    public static void initializeSouls()
    {
        _soulCounter = 1;
        _requiredSouls = 1;
    }
}
