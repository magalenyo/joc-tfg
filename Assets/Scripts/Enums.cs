﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums
{
    public enum AnimationsStates : int 
    {
        Idle = 0,
        Move = 1,
        CrouchIdle = 2,
        CrouchMove = 3,
        Jump = 4,
        StandAttack = 5,
        CrouchAttack = 6,
        Roll = 7
    }

    public enum ChestBehaviour : int
    {
        Close = 0,
        Open = 1,
        Shaking = 2
    }

}
