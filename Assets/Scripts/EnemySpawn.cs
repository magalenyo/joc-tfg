﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public string _enemyId;
    private Object _original;
    private GameObject _enemyInstance;
    private Enemy _enemy;
    private bool _useStaticRef = true;

    private void Awake()
    {
        if (_useStaticRef) _original = StaticResources.getEnemyPrefab(_enemyId);
    }

    public void spawnEnemy(int level)
    {
        if (_enemyInstance == null) //Creates the enemy if there's not an instance.
        {
            if (_original!= null) _enemyInstance = (GameObject)Instantiate(_original, transform);
            else _enemyInstance = (GameObject)Instantiate(Resources.Load("Enemies/" + _enemyId), transform);
            _enemy = _enemyInstance.GetComponent<Enemy>();
            _enemy.setLevel(level);
        }
        else
        {
            _enemy.respawn(transform.position); 
        }
    }

    public void destroyEnemy()
    {
        Destroy(_enemyInstance);
    }
}
