﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VideoSettings : MonoBehaviour
{
    private Resolution[] _resolutions;
    [SerializeField] private TMP_Dropdown _qualityDropdown;
    [SerializeField] private TMP_Dropdown _resolutionDropdown;

    void Start()
    {
        _resolutions = Screen.resolutions;
        setQuality();
        setResolutions();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void setQuality()
    {
        int qualityLevel = QualitySettings.GetQualityLevel();
        _qualityDropdown.value = qualityLevel;
        _qualityDropdown.RefreshShownValue();
    }

    public void setVideoQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void setFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void setResolution(int resolutionIndex)
    {
        Screen.SetResolution(_resolutions[resolutionIndex].width, _resolutions[resolutionIndex].height, Screen.fullScreen);
    }

    private void setResolutions()
    {
        int currentResIndex = 0;

        _resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        for(int i = 0; i < _resolutions.Length; i++)
        {
            options.Add(_resolutions[i].width + " x " + _resolutions[i].height + " - " + _resolutions[i].refreshRate +" hz");
            if(_resolutions[i].width == Screen.currentResolution.width && _resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        _resolutionDropdown.AddOptions(options);
        _resolutionDropdown.value = currentResIndex;
        _resolutionDropdown.RefreshShownValue();
    }
    //Edit->Project Settings->Player->StandAloneOptions->Display Resolution Dialogue->Disabled per treure dialog inicial de set resolution
   
}
