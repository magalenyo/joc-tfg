﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class TrapTrigger : MonoBehaviour
{

    public Trap _trap;

    private AudioSource _audioSource;
    [SerializeField] private AudioClip _clipOnClick;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == Constants.Tag_Player)
        {
            try
            {
                _trap.activateTrap();
                if (_clipOnClick != null) _audioSource.PlayOneShot(_clipOnClick);
            }
            catch(NullReferenceException e)
            {
                Console.WriteLine("ERROR: Trap is not assigned" + e.Message);
            }
        }
    }
}
