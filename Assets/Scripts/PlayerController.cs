﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Consumer
{
    #region Singleton
    public static PlayerController instance = null;
    #endregion

    #region Velocities
    [Header("Velocities")]

    [SerializeField] private float _speedNormal = 7;
    [SerializeField] private float _speedCrouch;        //half the regular speed
    private float _moveSpeed;
    [SerializeField] private float _jumpForce;
    #endregion

    #region Collider heights
    [Header("Collider heights")]

    [SerializeField] private float _standColliderHeight;
    [SerializeField] private float _crouchColliderHeight;
    [SerializeField] private float _standUpHeight;

    #endregion

    #region KeyCodes
    [Header("Key Codes")]

    [SerializeField] private KeyCode _keyCrouch;
    //[SerializeField] private KeyCode _keyMeeleAttack;
    [SerializeField] private KeyCode _keyInteract;
    [SerializeField] private KeyCode _keyRoll;
    [SerializeField] private KeyCode _keyPrimarySpell;
    [SerializeField] private KeyCode _keySecondarySpell;
    [SerializeField] private KeyCode _keyUseEquipedObj;
    //[SerializeField] private KeyCode _keyNextObj;

    [SerializeField] private int _primaryActionKey;
    //[SerializeField] private int _secondaryActionKey;
    #endregion

    #region Components
    [Header("Components")]

    public CharacterController _controller;
    public Animator _animator;
    public SpellController _spellController;
    //public AnimatorController _animatorController
    //public string _animNameMeleeAttack;

    #endregion

    #region Weapons
    [Header("Weapons")]
    [SerializeField] private GameObject _sword;
    [SerializeField] private MeleeWeaponController _swordController;
    [SerializeField] private GameObject _kunai;
    private ProjectileController _projectileController;
    #endregion

    #region Movement
    [Header("Movement")]
    private Vector3 moveDirection;
    //private float moveHorizontal;
    [SerializeField] private float gravityScale;
    [SerializeField] private float gravity = 13f;
    #endregion

    #region States
    private bool _isCrouch;
    private bool _isJumping;
    //private bool _isMoving; //DEPRECATED
    private bool _isMeleeAttacking;
    private bool _isRolling;
    private bool _isThrowingKunai;
    private bool _isCasting;
    //private bool _isDead;
    private string _consuming = "consuming";
    #endregion

    #region ShotPoints
    [Header("Shot Points")]
    [SerializeField] private Transform _shotPointLeftHand;
    //public Transform ShotPointKunai;

    #endregion

    #region Interactions

    private InteractableController _interactableController;

    #endregion

    #region Colisions
    private bool colision;

    #endregion

    #region Audio 
    [SerializeField] private int _nJumpLaunchClips;
    [SerializeField] private AudioClip[] _jumpLaunchClips;

    [SerializeField] private int _nJumpLandClips;
    [SerializeField] private AudioClip[] _jumpLandClips;

    [SerializeField] private AudioClip _rollClip;

    #endregion
    
    //public bool grounded = false; //per debug

    #region Anim Dictionaries
    //private Dictionary<string, bool> animBoolean;
    //private Dictionary<string, int> animLayer;
    //private Dictionary<string, float> animFloats;

    #endregion

    #region Respawn
    //Delegates
    public delegate void playerDeadAction();
    public static event playerDeadAction OnPlayerDead;

    public delegate void EquipmentChange(string cons, int count);
    public static event EquipmentChange OnEquipmentChange;


    #endregion

    #region Backpack
    private Backpack _backpack;
    #endregion

    #region Distances for raycast Object detection
    public float distanceFront = 1.5f;
    //public bool frontCollision;
    #endregion

    void Awake()
    {
        DontDestroyOnLoad(transform.root.gameObject);
        initializeStats();
        initializeSounds();
        initializeSpells();
        if(instance == null) instance = this;

    }

    protected override void Start()
    {

        
        base.Start();

        _interactableController = GetComponent<InteractableController>();

        _controller = GetComponent<CharacterController>();
        _controller.slopeLimit = 90;
        _animator = GetComponent<Animator>();
        //_animatorController = GetComponent<AnimatorController>();
        _controller.material.dynamicFriction = 0;
        _controller.material.staticFriction = 0;
        _controller.material.frictionCombine = 0;

        _standColliderHeight = _controller.height;
        _crouchColliderHeight = _controller.height / 2;
        _standUpHeight = 1.8f;

        _isCrouch = false;
        _isJumping = false;
        //_isMoving = false;
        _isMeleeAttacking = false;
        _isRolling = false;
        _isThrowingKunai = false;
        _isCasting = false;
        //_isDead = false;

        //_speedNormal = 7;
        _speedCrouch = _speedNormal * 0.6f;

        _keyCrouch = KeyCode.LeftControl;
        //_keyMeeleAttack = KeyCode.Q;
        _keyInteract = KeyCode.F;
        _keyRoll = KeyCode.LeftShift;
        _keyPrimarySpell = KeyCode.Q;
        _keySecondarySpell = KeyCode.E;
        _keyUseEquipedObj = KeyCode.Mouse1;


        _primaryActionKey = 0;
        //_secondaryActionKey = 1;


        _moveSpeed = _speedNormal;

        _swordController = _sword.GetComponent<MeleeWeaponController>();
        //_animNameMeleeAttack = "swordAttack";

        _projectileController = GetComponent<ProjectileController>();


        //Starter pack 
        _backpack = new Backpack(this);
        _backpack.addNObject(new KunaiConsumible(), 10);
        _backpack.equipNext();
        //OnEquipmentChange(_backpack.current().Item1.GetType().ToString(), _backpack.current().Item2);


        //Estructures animacions
        //animBoolean = new Dictionary<string, bool>();
        //animLayer = new Dictionary<string, int>();
        //animFloats = new Dictionary<string, float>();
        _actions[_consuming] = false;

        ExperienceSystem.initializeExperience();
        SoulsSystem.initializeSouls();
        StartCoroutine(prepareHandlers());  //Considerar canviar per una pantalla on doni temps a tots els starts
        
        
    }

    IEnumerator prepareHandlers()
    {
        yield return new WaitForSeconds(1 * Time.deltaTime);
        UIEventHandler.healthChanged(_stats.getStat(Constants.currentHealth), _stats.getStat(Constants.maxHealth));
        UIEventHandler.experienceGained(ExperienceSystem._playerExperience, ExperienceSystem._requiredExperience, ExperienceSystem._playerLevel);
        UIEventHandler.soulCollected(SoulsSystem._soulCounter);
        OnEquipmentChange(_backpack.current().Item1.GetType().ToString(), _backpack.current().Item2);
        // UIEventHandler.onEquipmentChanged += ;

    }


    public Transform getPosition()
    {
        return transform;
    }

    #region Calls per frame

    void Update()
    {

        if (!isDying() &&!isDead() && !LevelManager.gamePaused())
        {

            move();
            jump();
            roll();
            crouch();
            meleeAttack();
            primarySpell();
            secondarySpell();
            interactions();
            useObject();
            _controller.Move(moveDirection * Time.deltaTime);

        }
        updateLocalAnimations();
       
    }

    /// <summary>
    /// Makes player's position Z to 0 in case it has moved since the last frame
    /// </summary>
    void FixedUpdate()
    {
        if (!isDying() && !LevelManager.gamePaused())
        {
            //_controller.Move(moveDirection * Time.deltaTime);
            if (transform.position.z != 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
            }
        }
        //grounded = _controller.isGrounded;

    }



    #endregion

    #region UseObjects

    public void useObject()
    {

        if ((Input.GetAxis("Mouse ScrollWheel") > 0f))
        {
            _backpack.equipNext();
            OnEquipmentChange(_backpack.current().Item1.GetType().ToString(), _backpack.current().Item2);
        }

        if ((Input.GetAxis("Mouse ScrollWheel") < 0f))
        {
            _backpack.equipPrevious();
            OnEquipmentChange(_backpack.current().Item1.GetType().ToString(), _backpack.current().Item2);
        }

        if (Input.GetKeyDown(_keyUseEquipedObj) && !_actions[_consuming])
        {
            _actions[_consuming] = _backpack.consumeEquiped();
            OnEquipmentChange(_backpack.current().Item1.GetType().ToString(), _backpack.current().Item2);
        }

    }

    public override void drink()
    {
        _animLayer["Drink"] = 1;
        _kunai.SetActive(false);
    }

    public void event_endDrinking()
    {
        _animLayer["Drink"] = 0;
        _actions[_consuming] = false;
        _kunai.SetActive(true);

    }


    #endregion

    #region Animations controller
    protected void updateLocalAnimations()
    {
        _animFloats["Speed"] = Math.Abs(moveDirection.x);

        //_actions["isMoving"] = _isMoving;
        _actions["isJumping"] = _isJumping;
        _actions["isCrouching"] = _isCrouch;
        _actions["isRolling"] = _isRolling;
        _actions["isAttacking"] = _isMeleeAttacking;
        _actions["isThrowingKunai"] = _isThrowingKunai;
        _actions["isCasting"] = _isCasting;

        _animLayer["Weapon"] = 1;
        _animLayer["Kunai"] = 1;

        if (_isMeleeAttacking) _animLayer["Weapon"] = 1;
        else _animLayer["Weapon"] = 0;

        if (_isThrowingKunai) _animLayer["Kunai"] = 1;
        else _animLayer["Kunai"] = 1;

        if (_isCasting) _animLayer[_spellController.currentType()] = _isCasting ? 1 : 0;

        //Cal posar be els estats encara!!!
        updateAnimations();
    }
    #endregion

    #region Respawn

    /// <summary>
    /// Overrided function of Character, The player dies.
    /// </summary>
    protected override void die()
    {
        _actions[_dead] = true;
        _actions[_moving] = false;
        OnPlayerDead();
    }

    public override void teleportTo(Vector3 _position)
    {
        transform.position = _position;

        //Si ja s'ha fet el getComponent cal desactivar el controller.
        if (_controller != null)
        {
            _controller.enabled = false;
            transform.position = _position;
            _controller.enabled = true;
        }
    }
    public override void respawn(Vector3 _position)
    {
        teleportTo(_position);
        _actions[_isRespawning] = true;
    }
    /// <summary>
    /// The player resurrect.
    /// </summary>
    public void event_endRespawn()
    {
        _actions[_isRespawning] = false;
        resetStats();
    }

    /// <summary>
    /// True if player is dying.
    /// </summary>
    public bool isDying()
    {
        return _actions[_isDying];
    }

    /// <summary>
    /// True if player is dead.
    /// </summary>
    public bool isDead()
    {
        return _actions[_dead];
    }



    #endregion

    #region Movement
    /// <summary>
    /// Gets horizontal axis value, turns the player towards right if positive value, else negative value player's direction to left
    /// </summary>
    private void move()
    {
        float hAxis = 0;
        if (!_isRolling && !isDying() && !isDead())
        {
            hAxis = Input.GetAxis("Horizontal");
            if (hAxis < 0)
            {
                transform.LookAt(transform.position + Vector3.left);
                if (!canWalk()) hAxis = 0;
            }

            else if (hAxis > 0)
            {
                transform.LookAt(transform.position + Vector3.right);
                if (!canWalk()) hAxis = 0;
            }
        }
        if (hAxis != 0 && !isDying() && !isDead())
            _actions[_moving] = true;//_isMoving = true;
        else
            _actions[_moving] = false;

        moveDirection = new Vector3(hAxis * _moveSpeed, moveDirection.y, 0f);
    }
    #endregion

    #region Jump
    /// <summary>
    /// If player is grounded and not crouching and jump key pressed, adds force to player's y and jumps
    /// </summary>
    private void jump()
    {
        if (_controller.isGrounded && !_isCrouch && Input.GetButtonDown("Jump"))
        {
            gravityScale = 1;
            playAudio(_jumpLaunchClips);
            _isJumping = true;
            moveDirection.y = _jumpForce;
        }
        else if (_isJumping && _controller.isGrounded)
        {
            playAudio(_jumpLandClips);
            _isJumping = false;
            gravityScale = 1;

        }
        else if (!_isJumping && !_controller.isGrounded)
        {
            gravityScale = 10f;
        }

        else if (_isJumping && !_controller.isGrounded)
        {
            gravityScale += 0.1f;

        }

        moveDirection.y -= gravityScale * gravity * Time.deltaTime;
    }
    #endregion

    #region Player's collider controllers
    /// <summary>
    /// Set player's collider half it's normal height, also repositions the center
    /// </summary>
    private void setSmallCollider()
    {
        _controller.height = _crouchColliderHeight;
        _controller.center = new Vector3(_controller.center.x, _crouchColliderHeight / 2, _controller.center.z);
    }

    /// <summary>
    /// Set player's collider the normal height, also repositions the center
    /// </summary>
    private void setStandCollider()
    {
        _controller.height = _standColliderHeight;
        _controller.center = new Vector3(_controller.center.x, _standColliderHeight / 2, _controller.center.z);
    }
    #endregion

    #region Crouch
    /// <summary>
    /// If crouch button pressed and not jumping, player's collider set to small. Stands when the key is released. Also, if it's crouched and under a surface and key released, stays crouched
    /// </summary>
    private void crouch()
    {
        if (Input.GetKey(_keyCrouch) && !_isJumping)
        {
            _moveSpeed = _speedCrouch;
            if (!_isCrouch)
            {
                setSmallCollider();
                _isCrouch = true;
            }
        }

        else if (_isCrouch)
        {
            int layerMask = 1 << 9;
            layerMask = ~layerMask;

            colision = Physics.Raycast(transform.position, Vector3.up, _standUpHeight, layerMask);

            if (!colision)
            {
                setStandCollider();
                _isCrouch = false;
                _moveSpeed = _speedNormal;
            }
        }
    }
    #endregion

    #region Roll
    /// <summary>
    /// If not jumping, and not grounded and not rollin and roll key pressed, rolls and makes player's collider small
    /// </summary>
    private void roll()
    {
        if (!_isJumping && _controller.isGrounded && !_isRolling && Input.GetKeyDown(_keyRoll))
        {
            _audioSource.PlayOneShot(_rollClip);
            _isRolling = true;
            setSmallCollider();
        }
    }

    /// <summary>
    /// Called when roll animation ends, if is under a surface, stays crouched, else player's collider height set to normal
    /// </summary>
    public void event_endRolling()
    {
        _isRolling = false;

        int layerMask = 1 << 9;
        layerMask = ~layerMask;

        colision = Physics.Raycast(transform.position, Vector3.up, _standUpHeight, layerMask);

        if (!colision)
        {
            setStandCollider();
            _isCrouch = false;

        }
        else
        {
            setSmallCollider();
            _isCrouch = true;
        }
    }
    #endregion

    #region Melee attack

    /// <summary>
    /// If primary action key, attacks with the sword
    /// </summary>
    private void meleeAttack()
    {
        if (Input.GetMouseButtonDown(_primaryActionKey))
        {
            //_swordController.borrar();
            _isMeleeAttacking = true;
        }
    }

    /// <summary>
    /// Called when attack animation ends, stops attacking
    /// </summary>
    public void event_endAttacking()
    {
        _isMeleeAttacking = false;
        _swordController.stopAttack();
    }

    /// <summary>
    /// Called when attack animation is in progress
    /// </summary>
    public void event_swoosh()
    {
        _swordController.attack();
    }

    #endregion

    #region Kunai ranged attack

    /// <summary>
    /// If secondary action key, throwing kunai set to true
    /// </summary>
    public override void throwWeapon(GameObject projectilePref)
    {
        _actions[_consuming] = true;
        _isThrowingKunai = true;
        _projectileController.setProjPrefab(projectilePref);
    }

    public bool canThrow()
    {
        return !_isThrowingKunai;
    }

    /// <summary>
    /// Called from the animation, casts a kunai prefab from the left hand, also makes player's static kunai not visible
    /// </summary>
    public void event_throwKunai()
    {
        _projectileController.shootAssigned(_shotPointLeftHand);

        _kunai.SetActive(false);
    }

    /// <summary>
    /// Called when throw kunai animation ends, makes hand kunai visible
    /// </summary>
    public void event_endThrowingKunai()
    {
        _isThrowingKunai = false;
        _kunai.SetActive(true);
        _actions[_consuming] = false;

    }

    #endregion

    #region Spell System

    /// <summary>
    /// If primary spell key pressed, cast primary spell, hides the kunai
    /// </summary>
    private void primarySpell()
    {

        if (Input.GetKeyDown(_keyPrimarySpell) && !_isCasting)
        {
            _spellController._current = _spellController.Primary;

            if (_spellController.canCastCurrent())
            {

                Spell spell = _spellController.getCurrentSpell();
                _isCasting = true;
                _audioSource.PlayOneShot(spell.getCastAudio());

                //Utilitzat per canviar el layer de l'animacio segons el tipus d'spell

                _kunai.SetActive(false);
                _sword.SetActive(false);
            }
        }

    }

    /// <summary>
    /// If secondary spell key pressed, cast secondary spell
    /// </summary>
    private void secondarySpell()
    {
        if (Input.GetKeyDown(_keySecondarySpell) && !_isCasting)
        {

            //Spell rockstorm = new Spell_RockStorm(gameObject);
            //rockstorm.cast();
            _spellController._current = _spellController.Secondary;

            if (_spellController.canCastCurrent())
            {

                Spell spell = _spellController.getCurrentSpell();
                //spell.cast();
                _isCasting = true;
                _audioSource.PlayOneShot(spell.getCastAudio());

                //Utilitzat per canviar el layer de l'animacio segons el tipus d'spell

                _kunai.SetActive(false);
                _sword.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Called from Event handler when the spell is up to cast (started animation and passed some time). Casts the current spell
    /// </summary>
    public void useSpell()
    {

        _spellController.castCurrent();
    }

    /// <summary>
    /// Player is not casting, makes the kunai visible
    /// </summary>
    public void endCasting()
    {
        _isCasting = false;
        _animLayer[_spellController.currentType()] = 0;
        _kunai.SetActive(true);
        _sword.SetActive(true);
    }
    #endregion

    #region Interactions

    private void interactions()
    {
        if (Input.GetKey(_keyInteract))
            _interactableController.interact();
    }

    public override void giveObject(Consumible consumible, int amount)
    {
        _backpack.addNObject(consumible, amount);
        OnEquipmentChange(_backpack.current().Item1.GetType().ToString(), _backpack.current().Item2);

    }

    #endregion

    #region Initializations
    /// <summary>
    /// Sets player's base stats
    /// </summary>
    protected override void initializeStats()
    {
        Dictionary<string, float> stats = new Dictionary<string, float>();
        stats.Add(Constants.maxHealth, Constants.Stats_Player_maxHealth);
        stats.Add(Constants.currentHealth, Constants.Stats_Player_maxHealth);
        stats.Add(Constants.resMagic, Constants.Stats_Player_resMagic);
        stats.Add(Constants.resPhys, Constants.Stats_Player_resPhys);
        stats.Add(Constants.moveSpeed, Constants.Stats_Player_moveSpeed);
        stats.Add(Constants.cooldown, Constants.Stats_Player_cooldown);
        stats.Add(Constants.strength, Constants.Stats_Player_strength);
        stats.Add(Constants.magic, Constants.Stats_Player_magic);
        _stats = new CharacterStats(stats);
    }

    private void initializeSpells()
    {

        Spell fireball = new Spell_fireball(gameObject, _shotPointLeftHand);
        Spell rockstorm = new Spell_IceStorm(gameObject);

        List<Spell> allSpells = new List<Spell>();
        allSpells.Add(fireball);
        allSpells.Add(rockstorm);

        _spellController = new SpellController(allSpells);

        _spellController.assignSpell(_spellController.Primary, 0);
        _spellController.assignSpell(_spellController.Secondary, 1);


    }

    /// <summary>
    /// Sets player's base actions
    /// </summary>
    protected override void setExtraActions()
    {
        _actions[_consuming] = false;
        _actions[_isRespawning] = false;
        _actions[_dead] = false;
    }

    protected void initializeSounds()
    {
        //Jump launch
        _jumpLaunchClips = new AudioClip[_nJumpLaunchClips];
        for (int i = 1; i <= _nJumpLaunchClips; i++)
        {
            _jumpLaunchClips[i - 1] = Resources.Load<AudioClip>("sounds/move/jump_launch" + i);
        }

        //Jump land
        _jumpLandClips = new AudioClip[_nJumpLandClips];
        for (int i = 1; i <= _nJumpLandClips; i++)
        {
            _jumpLandClips[i - 1] = Resources.Load<AudioClip>("sounds/move/jump_land" + i);
        }
    }


    #endregion

    #region 
    public override void takeDamage(float amount)
    {
        base.takeDamage(amount);
        UIEventHandler.healthChanged(_stats.getStat(Constants.currentHealth), _stats.getStat(Constants.maxHealth));
    }



    public Spell getSpell(string spell)
    {
        return _spellController.getSpellByType(spell);
    }
    #endregion

    public float getPlayerCurrentCenter()
    {
        if (_controller == null) return 0.71f;
        else return _controller.center.y;
    }

    public bool isRespawning()
    {
        return _actions[_isRespawning];
    }

    public bool canWalk()
    {

        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, distanceFront))
        {
            if (hit.transform != null)
            {
                //frontCollision = hit.transform.tag == "Obstacle";
                if (hit.transform.tag == "Obstacle") return false;
            }
        }

        return true;
    }

}
