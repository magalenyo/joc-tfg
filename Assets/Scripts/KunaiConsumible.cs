﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KunaiConsumible : Consumible
{
    public override void consume(Consumer _consumer) {

        _consumer.throwWeapon((GameObject)Resources.Load("KunaiProjectileVariant", typeof(GameObject)));
     
    }
}
