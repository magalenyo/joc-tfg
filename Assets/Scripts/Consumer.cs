﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumer : Character
{
    public abstract void drink();
    public abstract void throwWeapon(GameObject projectilePref);
    public abstract void giveObject(Consumible consumible, int amount);
}
