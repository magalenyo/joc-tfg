﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestBehaviour : Interactable
{
    private Animator _animator;
    public GameObject _objectInside;
    private Collider _interactionCollider;

    void Start()
    {
       
        _animator = GetComponent<Animator>();
        _interactionCollider = GetComponent<Collider>();
      
        _objectInside.SetActive(false);
    }

    public void stopShake()
    {
        _animator.SetInteger("State", 0);
    }

    public void shake()
    {
       _animator.SetInteger("State", 1);
    }

    public void Open()
    {
        this._animator.SetInteger("State", 2);
        _interactionCollider.enabled = false;
    }

    public override void interact(Consumer _character)
    { 
        Open();
    }

    public void event_isOpen()
    {
        _objectInside.SetActive(true);
    }


}
