﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Seeker : Enemy
{
    [Header("Agent variables")]

    public Transform _target;
    public NavMeshAgent _agent;

    public float _distanceToTarget;
    public float _lookRadius;
    public float _jumpDistance;

    public bool _isJumping = false;
    private Vector3 target;//, startPosition;


    [Header("Seeker variables")]
    [SerializeField] private float _jumpForceVertical = 100f;
    [SerializeField] private float _jumpForceHorizontal = 40f;

    [SerializeField] private float _movementSpeed = 40f;
    [SerializeField] private float _smooth = 10f;
    [SerializeField] private float _repositionDistance = 4f;
    [SerializeField] private float _distanceToGround = 0.2f;


    [SerializeField] private MeleeWeaponController _weapon;
    [SerializeField] private AudioClip _attackClip; 


    private bool _isGrounded
    {
        get {
            return Physics.Raycast(transform.position, -Vector3.up, _distanceToGround + 0.1f);
        }
    }

    private void Awake()
    {
        initializeStats();

    }

    protected override void Start()
    {
        base.Start();


        setFootstepSounds("slime/SlimeStep");

        //initializeStats();
        _target = PlayerController.instance.transform;
        _agent = GetComponent<NavMeshAgent>();
        _isJumping = false;

        Physics.IgnoreLayerCollision(Constants.Layer_SlimePrefab, Constants.Layer_Player);
        Physics.IgnoreLayerCollision(Constants.Layer_SlimePrefab, Constants.Layer_Mascarea);
        Physics.IgnoreLayerCollision(Constants.Layer_SlimePrefab, Constants.Layer_SlimePrefab);
        Physics.IgnoreLayerCollision(Constants.Layer_SlimePrefab, Constants.Layer_Enemy);

    }

    void Update()
    {
        _distanceToTarget = Vector3.Distance(_target.position, transform.position);
        if (!isDying() && !isDead() && !isPlayerDead())
        {
            
            if (_agent.enabled && _agent.isStopped) _agent.isStopped = false;
            if (!_isJumping && _isGrounded)
            {
                move();

                faceTarget();
            }
            jump();
            meleeAttack();

            if(_distanceToTarget > _lookRadius)
            {
                reset();
            }
        }
        else
        {
            _actions[_moving] = false;
            if(_agent.enabled) _agent.isStopped = true;
        }
        
        
        updateAnimations();

    }


    private void meleeAttack()
    {
        if (_distanceToTarget <= _agent.stoppingDistance || _isJumping)
        {
            _actions[_attacking] = true;
            _weapon.attack();
        }
        else
        {
            _actions[_attacking] = false;
            _weapon.stopAttack();
        }
    }

    private void move()
    {
        if (_distanceToTarget <= _lookRadius && _distanceToTarget > _agent.stoppingDistance)
        {
            if (!_agent.enabled)
            {
                _agent.enabled = true;
            }
            if (_agent.isStopped)
            {
                _agent.isStopped = false;
            }
            
            _agent.SetDestination(_target.position);
            _actions[_moving] = true;
            _agent.isStopped = false;
        }
        else
        {
            if (_agent.enabled)
            {
                _agent.isStopped = true;
            }
            _actions[_moving] = false;
        }


        //_actions[_attacking] = false;
    }
    
    private void jump()
    {
        if (_distanceToTarget <= _jumpDistance && !_isJumping)
        {
            playAttackAudio();

            _isJumping = true;
            _actions[_moving] = false;
            
            _agent.isStopped = true;
            _agent.enabled = false;

            //startPosition = transform.position;
            if (transform.forward.x > 0)
            {
                target = _target.position + new Vector3(2, 0, 0);
                _rb.AddForce(new Vector3(_jumpForceHorizontal, _jumpForceVertical, 0), ForceMode.Impulse);
            }
            else
            {
                target = _target.position - new Vector3(2, 0, 0);
                _rb.AddForce(new Vector3(_jumpForceHorizontal, _jumpForceVertical, 0), ForceMode.Impulse); //_rb.AddRelativeForce(new Vector3(0, _jumpForceVertical, -_jumpForceHorizontal), ForceMode.Impulse);
            }

        }
        else if (_distanceToTarget > _repositionDistance && _isJumping && _isGrounded)
        {
            _isJumping = false;
        }
    }

    private void faceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void FixedUpdate()
    {
        if (!isDead() && !isPlayerDead())
        {
            if (_isJumping)
            {
                Vector3 desiredPosition = transform.position + transform.forward * Time.deltaTime * _movementSpeed;
                Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _smooth * Time.deltaTime);     //ha de tenir un smooth major (10f)
                transform.position = smoothedPosition;
            }

            if (transform.position.z != 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, 0f);

            }
        }
        else
        {
            _agent.enabled = false;
            _rb.velocity = new Vector3(0, 0, 0);
        }
       
    }

    private void reset()
    {
        _isJumping = false;
        _actions[_jumping] = false;
        _actions[_moving] = false;
    }

    #region Audio
    private void playAttackAudio()
    {
        _audioSource.PlayOneShot(_attackClip);
    }
    #endregion



    public override void teleportTo(Vector3 _position)
    {
        _agent.Warp(_position);
    }

    protected override void initializeStats()
    {
        Dictionary<string, float> stats = new Dictionary<string, float>();
        stats.Add(Constants.maxHealth, Constants.Stats_Seeker_maxHelth);
        stats.Add(Constants.currentHealth, Constants.Stats_Seeker_maxHelth);
        stats.Add(Constants.resMagic, Constants.Stats_Seeker_resMagic);
        stats.Add(Constants.resPhys, Constants.Stats_Seeker_resPhys);
        stats.Add(Constants.moveSpeed, Constants.Stats_Seeker_moveSpeed);
        stats.Add(Constants.strength, Constants.Stats_Seeker_strength);
        stats.Add(Constants.experience, Constants.Stats_Seeker_experience);
        stats.Add(Constants.soulCounter, Constants.Stats_Seeker_souls);
        stats.Add(Constants.cooldown, 0);
        stats.Add(Constants.magic, 0);

        _stats = new CharacterStats(stats);
    }

    protected override void setExtraActions()
    {
        
    }
}
