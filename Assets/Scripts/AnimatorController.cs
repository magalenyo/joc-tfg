﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    public Animator _animator;
  
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void updateAnimations(Dictionary<string, bool> animBoolean, Dictionary<string, int> animLayer, Dictionary<string, float> animFloats)
    {
        foreach (KeyValuePair<String, bool> element in animBoolean)
            _animator.SetBool(element.Key,element.Value);

        if (animLayer != null)
            foreach (KeyValuePair<String, int> element in animLayer)
                _animator.SetLayerWeight(_animator.GetLayerIndex(element.Key), element.Value);
        if (animFloats != null)
            foreach (KeyValuePair<String, float> element in animFloats)
                _animator.SetFloat(element.Key, element.Value);
    }
}
