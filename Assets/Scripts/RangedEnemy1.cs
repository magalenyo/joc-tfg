﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using System;

/// <summary>
/// Class that controls the behaviour of the ranged enemy
/// </summary>
public class RangedEnemy1 : Enemy
{
    //public GameObject _player;
    public Transform _target;
    //public bool _playerInRange;


    //private MeleeWeaponController _weapon;

    public float _distanceToTarget;
    public float _lookRadius;

    private ProjectileController _projectileController;
    public GameObject _axeR;
    public GameObject _axeL;
    public Transform _shotPointRightHand;
    public Transform _shotPointLeftHand;

    //IA
    public NavMeshAgent _agent;

    public float _cooldown = 3;
    private float _nextReadyTime;

    private bool _mirrorThrow;

    //[SerializeField]
    //protected AudioClip[] _clips;
    //private AudioSource _audioSource;

    /// <summary>
    /// Initialize stats
    /// </summary>
    private void Awake()
    {
        initializeStats();
        _audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Initializes values
    /// </summary>
    protected override void Start()
    {
        base.Start();
        _projectileController = GetComponent<ProjectileController>();
        _target = PlayerController.instance.getPosition();              //gets the player position

        //_playerInRange = false;
        _agent = GetComponent<NavMeshAgent>();

        _mirrorThrow = true;
        _actions["isLeftThrow"] = _mirrorThrow;                         //inital state for attack animation
    }

    /// <summary>
    /// Checks if is close enough to the player to attack and if it is, it attacks it. If not able to attack but in range, it moves closer to the player.
    /// </summary>
    void Update()
    {

        if (!isDying() && !isDead() && !isPlayerDead())
        {
            if (_agent.isStopped) _agent.isStopped = false;

            //potAtacar = _distanceToTarget > _agent.stoppingDistance;
            _distanceToTarget = Vector3.Distance(_target.position, transform.position);
            if (!isDead() && !isPlayerDead())
            {
                if (!_actions[_attacking]) move();
                // if (_distanceToTarget <= _lookRadius && _distanceToTarget > _agent.stoppingDistance) faceTarget();
                faceTarget();
                attack();
                //faceTarget();
            }
        }
        else
        {
            _agent.isStopped = true;
            _actions[_moving] = false;
        }
        updateAnimations();
       
    }

    private void FixedUpdate()
    {
        if (transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        }
    }


    /// <summary>
    /// Moves if the player is inside the looking radius and current distance to target > stopping distance.
    /// </summary>
    private void move()
    {

        if (_distanceToTarget <= _lookRadius && _distanceToTarget > _agent.stoppingDistance)
        {
            _agent.SetDestination(_target.position);
            _actions[_moving] = true;
            _agent.isStopped = false;
        }
        else
        {
            _agent.isStopped = true;
            _actions[_moving] = false;
        }

        _actions[_attacking] = false;
        _animFloats[_speed] = Math.Abs(_agent.speed);
    }

    /// <summary>
    /// If is close enough to attack and time has passed since the last time it attacked. Attacks.
    /// </summary>
    private void attack()
    {
        _actions["isLeftThrow"] = _mirrorThrow;
        if (!_actions[_attacking] && _distanceToTarget <= _agent.stoppingDistance && Time.time > _nextReadyTime)
        {
            _actions[_attacking] = true;
        }
    }

    /// <summary>
    /// Looks towrds target
    /// </summary>
    void faceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    #region Axe ranged attack

    /// <summary>
    /// Sets the animation layer to attack
    /// </summary>
    private void rangedAttack()
    {
        _actions[_attacking] = true;
        
    }
    #endregion

    #region Events

    /// <summary>
    /// Called from the animation, casts an axe prefab from one shot point at the enemy's position, aiming towards the player.
    /// The shot point is RightHand if mirrorThrow is True, else the shot point is the LeftHand. Also, hides (visible to false) the throwing axe from the enemy.
    /// </summary>
    public void event_throwAxe()
    {
        _projectileController.shoot((GameObject)Resources.Load("AxeProjectile", typeof(GameObject)), _mirrorThrow ? _shotPointRightHand.position : _shotPointLeftHand.position, _target.position);
        if(_mirrorThrow) _axeL.SetActive(false);
        else _axeR.SetActive(false);

        //AudioClip clip = getRandomClip();
        //_audioSource.PlayOneShot(clip);

    }

    //private AudioClip getRandomClip()
    //{
    //    return _clips[UnityEngine.Random.Range(0, _clips.Length)];
    //}

    /// <summary>
    /// Called when throw axe animation ends. Resets variables and sets the missing axe to visible.
    /// </summary>
    public void event_endThrowAxe()
    {
        _actions[_attacking] = false;
        if(_mirrorThrow) _axeL.SetActive(true);
        else _axeR .SetActive(true);
        if (!_mirrorThrow) _mirrorThrow = true;
        else _mirrorThrow = false;
        _nextReadyTime = _cooldown + Time.time;
    }

    #endregion

    #region Debug
    //Debug, dibuixem l'area d'awake
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, _lookRadius);
    }
    #endregion

    #region Initializing
    /// <summary>
    /// Initializes stats
    /// </summary>
    protected override void initializeStats()
    {
        Dictionary<string, float> stats = new Dictionary<string, float>();
        stats.Add(Constants.maxHealth, Constants.Stats_Fighter_maxHealth);
        stats.Add(Constants.currentHealth, Constants.Stats_Fighter_maxHealth);
        stats.Add(Constants.resMagic, Constants.Stats_Fighter_resMagic);
        stats.Add(Constants.resPhys, Constants.Stats_Fighter_resPhys);
        stats.Add(Constants.moveSpeed, Constants.Stats_Fighter_moveSpeed);
        stats.Add(Constants.cooldown, Constants.Stats_Fighter_cooldown);
        stats.Add(Constants.strength, Constants.Stats_Fighter_strength);
        stats.Add(Constants.magic, Constants.Stats_Fighter_magic);

        stats.Add(Constants.experience, Constants.Stats_AxeThrower_experience);
        stats.Add(Constants.soulCounter, Constants.Stats_AxeThrower_souls);
        _stats = new CharacterStats(stats);
    }
    /// <summary>
    /// Sets no extra actions
    /// </summary>
    protected override void setExtraActions()
    {

    }

    #endregion

    /// <summary>
    /// Teleports this GameObject to position.
    /// </summary>
    public override void teleportTo(Vector3 _position)
    {
        _agent.Warp(_position);
    }
}
