﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeFloorTrap : Trap
{
    public GameObject _go;

    public override void activateTrap()
    {
        _go.SetActive(false);
    }
}
