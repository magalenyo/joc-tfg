﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class RespawnController : MonoBehaviour
{
    private List<EnemySpawn> _enemySpawnList;
    private PlayerController _playerController;
    private bool used; //Checks if respawn point was activated
    private LevelManager _levelManager;
    private bool _allEnemiesSpawned;

    private int _level;

    /// <summary>
    /// Initializes values
    /// </summary>
    private void Start()
    {
        getAllSpawns();
        _allEnemiesSpawned = false;

        _levelManager = LevelManager.instance;
        _levelManager.addSpawn(this);

        _playerController = PlayerController.instance;
        used = false;
    }

    /// <summary>
    /// Adds all the enemy spawners and adds them to the enemy spawn list
    /// </summary>
    private void getAllSpawns()
    {
        _enemySpawnList = new List<EnemySpawn>();
        foreach (Transform child in transform)
        {
            if (child.tag == "EnemySpawn")
                _enemySpawnList.Add(child.gameObject.GetComponent<EnemySpawn>());
        }
    }

    /// <summary>
    /// Spawn all enemies of the enmemySpawns childs
    /// </summary>
    public void spawnAllEnemies(int level)
    {
        _level = level;
        _allEnemiesSpawned = true;
        if (_enemySpawnList != null)
            foreach (var spawn in _enemySpawnList) spawn.spawnEnemy(level);

    }

    /// <summary>
    /// Spawns the player
    /// </summary>
    public void spawnPlayer()
    {
        _playerController.respawn(transform.position);
        print(transform.position);
    }

    /// <summary>
    /// When the player enters the trigger, saves this spawner as the new respawn point, and in case all the enemies are not spawned, it spawns them
    /// </summary>
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            if (!used)
            {
                used = true;
                _levelManager.setRespawnPoint(this);
                if (!_allEnemiesSpawned) spawnAllEnemies(_level);
            }
        }
    }

    /// <summary>
    /// Destroys all the enemies binded to this spawn
    /// </summary>
    public void destroyAllEnemies()
    {
        foreach (var spawn in _enemySpawnList)
        {
            spawn.destroyEnemy();
        }
    }
}
