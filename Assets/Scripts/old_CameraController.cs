﻿using UnityEngine;

/// <summary>
/// Main Camera behaviour; it follows the player at an offset distance, and also has a smooth swing
/// </summary>
public class old_CameraController : MonoBehaviour
{
    public PlayerController _player;       //Player's position
    public Vector3 _offset;         //Camera's offset
    public bool  useSmooth;

    //[Range(0,1)]
    public float _smooth;           //Follow smoothness

    void Start()
    {
        _player = PlayerController.instance;
        _smooth = 10.5f;
   
    }

    /// <summary>
    /// Lerps camera's position into next player position, also, looks at player
    /// </summary>
    void LateUpdate()
    {
        if (_player != null && !_player.isDying() || _player.isDead())
        {

            if (_player != null)
            {
                Vector3 desiredPosition = _player.transform.position + _offset;
                Vector3 smoothedPosition;

                if (useSmooth) smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _smooth * Time.deltaTime);     //ha de tenir un smooth major (10f)
                else smoothedPosition = desiredPosition;
                transform.position = smoothedPosition;

                transform.LookAt(_player.transform);
            }
                
        }
        
    }
}
