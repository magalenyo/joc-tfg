﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadMenu : MonoBehaviour
{
    public static bool _gamePaused;
    public GameObject _deadMenuUI;

    void Start()
    {
        _gamePaused = false;
    }
    //needs to be called from player
    public void pause()
    {
        _gamePaused = true;
        _deadMenuUI.SetActive(true);
        Time.timeScale = 0; //pause game
    }

    public void resume()
    {
        _gamePaused = false;
        Time.timeScale = 1;
    }

}
