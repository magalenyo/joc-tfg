﻿using System.Collections.Generic;
using System;

public class CharacterStats
{
    public Dictionary<string,float> _characterStats;

    public CharacterStats(Dictionary<string,float> newStats){
        _characterStats = newStats;
    }

    public float getStat(string stat)
    {
        try
        {
            return _characterStats[stat];
        }
        catch (KeyNotFoundException e)
        {
            Console.WriteLine("Error[SET]: stat does not exist", e.Source);
            return -1;
        }

    }  
    
    public void setStat(string stat, float value)
    {
        try
        {
            _characterStats[stat] = value;
        }
        catch (KeyNotFoundException e)
        {
            Console.WriteLine("Error[SET]: stat does not exist", e.Source);
        }
    }

    public void increaseStat(string stat, float value)
    {
        try
        {
            _characterStats[stat] += value;
        }
        catch(KeyNotFoundException e)
        {
            Console.WriteLine("Error[INCREASE]: stat does not exist", e.Source);
        }

      
    }

    public void decreaseStat(string stat, float value)
    {
        try
        {
            _characterStats[stat] -= value;
            if (_characterStats[stat] < 0) _characterStats[stat] = 0;
        }
        catch (KeyNotFoundException e)
        {
            Console.WriteLine("Error[DECREASE]: stat does not exist", e.Source);
        }
    }

    public void increaseStats(float factor)
    {
        foreach (KeyValuePair<string,float> kp in _characterStats)
        {
            increaseStat(kp.Key, factor);
        }
    }
}
