﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pause Menu Controller
/// </summary>
public class PauseMenu : MonoBehaviour
{
    public static bool _gamePaused;
    private bool _navigating;

    [Header("UIs")]
    [SerializeField] private GameObject _pauseMenuUI;
    [SerializeField] private GameObject _experienceUI;

    [Header("TabMenu")]
    [SerializeField] private TabMenu _tabMenu;

    /// <summary>
    /// Initializes values.
    /// </summary>
    void Start()
    {
        _gamePaused = false;
        _navigating = false;
    }

    /// <summary>
    /// If the pause menu key is pressed and the game is not tabed. If the game was paused and was not navigating, it resumes the game. Else if not gamepaused, it pauses it.
    /// </summary>
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !_tabMenu.isTabed())
        {
            if (_gamePaused && !_navigating)
            {
                resume();
            }
            else if(!_gamePaused)
            {
                pause();
            }
        }
    }

    /// <summary>
    /// Enables the basic GUI. Disables the pausemenu and experience UI and unfreezes the game: timeScale = 1
    /// </summary>
    public void resume()
    {
        UIEventHandler.enableBasicGUI();
        _pauseMenuUI.SetActive(false);
        _experienceUI.SetActive(false);
        Time.timeScale = 1; //pause game
        _gamePaused = false;
    }

    /// <summary>
    /// Disables the basic GUI and enables the pausemenu and ExperienceUI. Freezes the game: timeScale = 0
    /// </summary>
    public void pause()
    {
        UIEventHandler.disableBasicGUI();
        _pauseMenuUI.SetActive(true);
        _experienceUI.SetActive(true);
        Time.timeScale = 0; //pause game
        _gamePaused = true;
    }

    /// <summary>
    /// Clears the timeScale and sets it to 1
    /// </summary>
    public void exitToMainMenu()
    {
        Time.timeScale = 1;
    }

    /// <summary>
    /// Sets navigating to value
    /// </summary>
    public void setNavigating(bool value)
    {
        _navigating = value;
    }

    /// <summary>
    /// Returns true if the game is paused.
    /// </summary>
    public bool isPaused()
    {
        return _gamePaused;
    }

}
