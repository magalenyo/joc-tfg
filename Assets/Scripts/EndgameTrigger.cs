﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EndgameTrigger : MonoBehaviour
{
    public GameObject _UI;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == Constants.Tag_Player)
        {
            Time.timeScale = 0;
            _UI.SetActive(true);
        }
    }
}
