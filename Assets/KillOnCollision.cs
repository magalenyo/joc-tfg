﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnCollision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Constants.Tag_Enemy || other.tag == Constants.Tag_Player)
        {
            other.GetComponent<Character>().takeDamage(int.MaxValue);
        }

    }
}
