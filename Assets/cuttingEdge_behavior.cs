﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cuttingEdge_behavior : MonoBehaviour
{
    public float speed = 2f;
    public float angle = 40f;
    public bool startWithComplementary = false;
    private float deltaE = 5f;
    private float targetAngle;
    
    private float angleR;
    private float angleL;

    

    private void Start()
    {
        targetAngle = angle;
        angleR = targetAngle;
        angleL = 360 - angleR; //Calculem el complementari.

        if (startWithComplementary) targetAngle = angleL;
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion newRotation = Quaternion.AngleAxis(targetAngle, Vector3.right);
        transform.rotation = Quaternion.Slerp(transform.rotation , newRotation, speed * Time.deltaTime);

        if (inRange(transform.rotation.eulerAngles.x))
            changeTarget();
    }

    private bool inRange(float angle)
    {
        return ((angle < targetAngle + deltaE) && (angle > targetAngle - deltaE));
    }

   private void changeTarget()
    {
        if (targetAngle == angleR) targetAngle = angleL;
        else targetAngle = angleR;
    }
}
